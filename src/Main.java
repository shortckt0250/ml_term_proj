import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import core.genetic_algo.Attribute;
import core.genetic_algo.Attribute.AttributeType;
import core.genetic_algo.ContniousValueConvertor;
import core.genetic_algo.Fitness;
import core.genetic_algo.FitnessAccuracySize;
import core.genetic_algo.GeneticAlgorithm;
import core.genetic_algo.GeneticAlgorithm.CrossoverType;
import core.genetic_algo.GeneticAlgorithm.SelectionStrategy;
import core.genetic_algo.Hypothesis;
import core.genetic_algo.Range;
import core.genetic_algo.Rule;
import core.neural_networks.ANN_System;
import core.neural_networks.DiscreteInputOutput;
import core.neural_networks.Example;
import core.neural_networks.InputAndWeightLengthMismatchExeption;


public class Main {
    
    public static double VALIDATION_PROPORTION = 0.33;
    
    public static class Options {
        public enum RunType {
            testIrisParamTuneOnly, testIrisParamTuneFeature, testIrisParamTuneFeatureWeight, testIrisGProp,
            testBCancerParamTuneOnly, testBCancerParamTuneFeature, testBCancerParamTuneFeatureWeight, testBCancerGProp
            
        }
        
        RunType rtype = null;
        Double fitnessThreshold = null; //-ft
        Integer maxIter = null; //-max
        Integer p = null;   // -p
        Double r = null;    // -r
        Double m = null;    // -m
        Integer conv = null; // -conv
        SelectionStrategy selection = null; // -select
        CrossoverType cross = null;         // -cross 
    }
    
    public static Options parseArgs(String[] args) {
        Options opt = new Options();
        HashMap<String, String> argMap = new HashMap<String, String>();
        String irisTestParamTune = "-ip";
        String irisTestParamTuneFeature = "-ipf";
        String irisTestParamTuneFeatureWeight = "-ipfw";
        String irisTestGProp = "-ig";
        String bCancerTestParamTune = "-bp";
        String bCancerTestParamTuneFeature = "-bpf";
        String bCancerTestParamTuneFeatureWeight = "-bpfw";
        String bCancerTestGProp = "-bg";
        
        for(int i=0; i < args.length; ){
            if(args[i].trim().equalsIgnoreCase(irisTestParamTune) ||
                    args[i].trim().equalsIgnoreCase(irisTestParamTuneFeature) ||
                    args[i].trim().equalsIgnoreCase(irisTestParamTuneFeatureWeight) ||
                    args[i].trim().equalsIgnoreCase(irisTestGProp) ||
                    args[i].trim().equalsIgnoreCase(bCancerTestParamTune) ||
                    args[i].trim().equalsIgnoreCase(bCancerTestParamTuneFeature) ||
                    args[i].trim().equalsIgnoreCase(bCancerTestParamTuneFeatureWeight) ||
                    args[i].trim().equalsIgnoreCase(bCancerTestGProp)) {
                argMap.put(args[i], "");
                i += 1;
            } else {
                if(args[i].charAt(0) == '-') {
                    if(i+1 >= args.length) {    // wrong options
                        PrintUsage();
                        return null;
                    }
                    argMap.put(args[i], args[i+1]);
                    i += 2;
                } else {
                    i += 1;
                }
            }
        }
        
        if(argMap.containsKey(irisTestParamTune)) {
            opt.rtype = Options.RunType.testIrisParamTuneOnly;
        } else if(argMap.containsKey(irisTestParamTuneFeature)) {
            opt.rtype = Options.RunType.testIrisParamTuneFeature;
        } else if(argMap.containsKey(irisTestParamTuneFeatureWeight)) {
            opt.rtype = Options.RunType.testIrisParamTuneFeatureWeight;
        } else if(argMap.containsKey(irisTestGProp)) {
            opt.rtype = Options.RunType.testIrisGProp;
        } else if(argMap.containsKey(bCancerTestParamTune)) {
            opt.rtype = Options.RunType.testBCancerParamTuneOnly;
        } else if(argMap.containsKey(bCancerTestParamTuneFeature)) {
            opt.rtype = Options.RunType.testBCancerParamTuneFeature;
        } else if(argMap.containsKey(bCancerTestParamTuneFeatureWeight)) {
            opt.rtype = Options.RunType.testBCancerParamTuneFeatureWeight;
        } else if(argMap.containsKey(bCancerTestGProp)) {
            opt.rtype = Options.RunType.testBCancerGProp;
        }else {
            PrintUsage();
            return null;
        }
        
        if(argMap.containsKey("-ft")) {
            try {
                opt.fitnessThreshold = Double.parseDouble(argMap.get("-ft"));
            } catch(Exception ex) {
                
            }
        }        
        if(argMap.containsKey("-max")) {
            try {
                opt.maxIter = Integer.parseInt(argMap.get("-max"));
            } catch(Exception ex) {
                
            }
        } 
        if(argMap.containsKey("-p")) {
            try {
                opt.p = Integer.parseInt(argMap.get("-p"));
            } catch(Exception ex) {
                
            }
        } 
        if(argMap.containsKey("-r")) {
            try {
                opt.r = Double.parseDouble(argMap.get("-r"));
            } catch(Exception ex) {
                
            }
        } 
        if(argMap.containsKey("-m")) {
            try {
                opt.m = Double.parseDouble(argMap.get("-m"));
            } catch(Exception ex) {
                
            }
        } 
        if(argMap.containsKey("-conv")) {
            try {
                opt.conv = Integer.parseInt(argMap.get("-conv"));
            } catch(Exception ex) {
                
            }
        } 
        if(argMap.containsKey("-select")) {
            try {
                int tmp = Integer.parseInt(argMap.get("-select"));
                switch(tmp) {
                case 1:
                    opt.selection = SelectionStrategy.Fitness_Proportion;
                    break;
                case 2:
                    opt.selection = SelectionStrategy.Tornament;
                    break;
                case 3:
                    opt.selection = SelectionStrategy.Rank;
                    break;
                };
            } catch(Exception ex) {
                
            }
        } 
        if(argMap.containsKey("-cross")) {
            try {
                int tmp = Integer.parseInt(argMap.get("-cross"));
                switch(tmp) {
                case 1:
                    opt.cross = CrossoverType.SinglePoint;
                    break;
                case 2:
                    opt.cross = CrossoverType.TwoPoint;
                    break;
                };
            } catch(Exception ex) {
                
            }
        } 

        return opt;
    }
    
    private static void PrintUsage() {
        System.out.print("\nUsage: java Main Type \n" + 
                            "  Type:\n" + 
                            "\t -ip \t testIrisParameterTuneOnly is run.\n" +
                            "\t -ipf \t testIrisParameterTuneFeature is run.\n" +
                            "\t -ipfw \t testIrisParameterTuneFeatureWeight is run.\n" +
                            "\t -ig \t testIrisGProp is run.\n" +
                            "\t -bp \t testBCancerParameterTuneOnly is run.\n" +
                            "\t -bpf \t testBCancerParameterTuneFeature is run.\n" +
                            "\t -bpfw \t testBCancerParameterTuneFeatureWeight is run.\n" +
                            "\t -bg \t testBCancerGProp is run.\n" + /*
                            "\n  Options:\n"  + 
                            "\t -ft \t to specify the fitnessThreshold to be used for stoping criteria.\n" +
                            "\t -max \t to specify maxIter or max number of generation befor stopping. If -ft is also specified then fitnessThreshold is first used untill maxIteration is reached. If not specified default value will be used\n" + 
                            "\t -p \t to specify population size. If not specified default value will be used\n" + 
                            "\t -r \t to specify replacement rate. If not specified default value will be used when required.\n" + 
                            "\t -m \t to specify mutation rate. If not specified default value will be used when required.\n" + 
                            "\t -conv \t to specify number of iterations to consider for convergence. For example if -conv 10 , then learning algorithm will check if the best hypothesis remains unchanged for 10 generations and stop if that is the case. If not specified default value 20% of maxIter will be used.\n" +
                            "\t -select NUMBER \t to specify selection strategy for testTennis and testIris. Use:\n" +
                                    "\t                \t NUMBER=1 for fitnessProportional\n" +
                                    "\t                \t NUMBER=2 for Tornament\n" +
                                    "\t                \t NUMBER=3 for Rank\n" +
                            "\t -cross NUMBER \t to specify crossover strategy. Use NUMBER=1 for SinglePoint, \n" +
                                    "\t                \t NUMBER=1 for Single Point Crossover\n" +
                                    "\t                \t NUMBER=2 for Two Point Crossover\n" + */
                            "\n");        
    }
    
    
    public static void main(String[] args) throws InputAndWeightLengthMismatchExeption {
        
        Options opts = parseArgs(args);
        if(opts == null)
            return;
        if(opts.rtype == Options.RunType.testIrisParamTuneOnly) {
            testIris();
        } else if(opts.rtype == Options.RunType.testIrisParamTuneFeature) {
            testIrisWithFeatureSelection();
        } else if(opts.rtype == Options.RunType.testIrisParamTuneFeatureWeight) {
            testIrisWithWeightAndFeatureSelection();
        } else if(opts.rtype == Options.RunType.testIrisGProp) {
            testIrisPrevPaper();   
        } else if(opts.rtype == Options.RunType.testBCancerParamTuneOnly) {
            testBreastCancer();
        } else if(opts.rtype == Options.RunType.testBCancerParamTuneFeature) {
            testBreastCancerWithFeatureSelection();
        } else if(opts.rtype == Options.RunType.testBCancerParamTuneFeatureWeight) {
            testBreastCancerWithWeightAndFeatureSelection();
        } else if(opts.rtype == Options.RunType.testBCancerGProp) {
            testBreastCancerPrevPaper();
        } else {
            PrintUsage();
        }
    }
    
    public static void testIris() throws InputAndWeightLengthMismatchExeption {
        Random rng = new Random(57492519);
        
        String trainFilename = "iris-train.txt";
        String testFilename = "iris-test.txt";
        
        List<Attribute> attrList = new ArrayList<Attribute>();
        int index = 0;
        
        // number of Hidden nodes attribute
        ContniousValueConvertor contValConvHidden = new ContniousValueConvertor(0, 6);  // 
        Attribute attrHidden = new Attribute(ANN_System.HIDDEN, new Range<Double>(1.0, true, 10.0, true), AttributeType.condition, index, 
                contValConvHidden, contValConvHidden.TOTAL_LEN) ;
        index += attrHidden.getLengthOfValueBits();
        attrList.add(attrHidden);
        
        // LEARNING_RATE attribute 
        ContniousValueConvertor contValConvLearningRate = new ContniousValueConvertor(4,14);
        Attribute attrLearningRate = new Attribute(ANN_System.LEARNING_RATE, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                contValConvLearningRate, contValConvLearningRate.TOTAL_LEN) ;
        index += attrLearningRate.getLengthOfValueBits();
        attrList.add(attrLearningRate);
        
        // MOMENTUM
        ContniousValueConvertor contValConvMomentum = new ContniousValueConvertor(4,14);
        Attribute attrMomentum = new Attribute(ANN_System.MOMENTUM, new Range<Double>(0.0 , true, 0.9, true), AttributeType.condition, index, 
                contValConvMomentum, contValConvMomentum.TOTAL_LEN) ;
        index += attrMomentum.getLengthOfValueBits();
        attrList.add(attrMomentum);
        
        // STOP_CRITERIA
        ContniousValueConvertor contValConvStopCriteria = new ContniousValueConvertor(0, 15);
        Attribute attrStopCriteria = new Attribute(ANN_System.STOP_CRITERIA, new Range<Double>(5000.0, true, 10000.0, true), AttributeType.condition, index, 
                contValConvStopCriteria, contValConvStopCriteria.TOTAL_LEN) ;
        index += attrStopCriteria.getLengthOfValueBits();
        attrList.add(attrStopCriteria);
        
        
        
        List<String> targetValuesList = new ArrayList<String>();
        targetValuesList.add("Iris-setosa");
        targetValuesList.add("Iris-versicolor");
        targetValuesList.add("Iris-virginica");
        
        
        Scanner train;
        try {    
            train = new Scanner(new File(trainFilename));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> tmpExamples = new ArrayList<Example>();
        List<Example> trainingSet = new ArrayList<Example>();
        List<Example> validationSet = new ArrayList<Example>();
        System.out.println("Reading traing data...");
        
        while(train.hasNext()) {
            double[] input = new double[4];
            input[0] = train.nextDouble();
            input[1] = train.nextDouble();
            input[2] = train.nextDouble();
            input[3] = train.nextDouble();
            
            String tmpTarget = train.next();
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            tmpExamples.add(e);
            if(rng.nextDouble() < (1.0-VALIDATION_PROPORTION)) {
                trainingSet.add(e);
            } else {
                validationSet.add(e);
            }
        } 
        
        /////// A Neural Networks layers
        int layers = 3;
        int[] numUnitsInLayers = new int[layers];
        numUnitsInLayers[0] = 4;    // input layer
        // hidden layer will be set by the GA
        numUnitsInLayers[2] = targetValuesList.size();
        
        
        // Initialise Genetic Algorithm
        Fitness fitness = new FitnessAccuracySize(); 
        Double fitnessThreshold = null; // stop when fitnessThreshold of 90% or more has been achieved on training data.
        int maxIter = 10;
        Integer convergeAfterIter = null;//(int) (0.2 * maxIter);  // 20% of maxIteration 
        int p = 30;
        double r = 0.6; 
        double m = 0.01;
        int ruleLength = Rule.calculateRuleLength(attrList);
        int initialNumOfRules = 1;                          // Only need one rule.
        SelectionStrategy select = SelectionStrategy.Tornament;
        CrossoverType cross = CrossoverType.Uniform;
        
        GeneticAlgorithm ga = new GeneticAlgorithm(fitness, fitnessThreshold, p, r, m, ruleLength, 
                select, cross, attrList, initialNumOfRules, true, 
                maxIter, convergeAfterIter, numUnitsInLayers);
        
        
        // Learn using Genetic Algorithm
        Hypothesis learnedHypothesis = ga.learn(trainingSet, validationSet, targetValuesList);
        System.out.printf("Learned Hypothesis:\n%s\n", learnedHypothesis.toString());
        
        
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 10000;
        
        if(learnedHypothesis.getValueOf(ANN_System.HIDDEN) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.HIDDEN);
            numUnitsInLayers[1] = (int) tmp;
        }
        if(learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE) != null) {
            learningRate = (Double) learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE);
        }
        if(learnedHypothesis.getValueOf(ANN_System.MOMENTUM) != null) {
            momentum = (Double) learnedHypothesis.getValueOf(ANN_System.MOMENTUM);
        }
        if(learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA);
            stopCriteria = (int) tmp;
        }
        
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(learnedHypothesis.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = learnedHypothesis.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn the ANN
        ann.learn(trainingSet, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        double trainingAccuracy = ann.predictionAccuracy(tmpExamples);
        
        System.out.println(ann.toString());
        
        System.out.printf("Validation Acc = %.3f\n", ann.predictionAccuracy(validationSet));
        System.out.printf("Train Acc = %.3f\n", trainingAccuracy);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // predict on test set
        Scanner test;
        try {
            test = new Scanner(new File(testFilename));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> testSet = new ArrayList<Example>();
        while(test.hasNext()) {
            double[] input = new double[4];
            input[0] = test.nextDouble();
            input[1] = test.nextDouble();
            input[2] = test.nextDouble();
            input[3] = test.nextDouble();
            
            String tmpTarget = test.next();
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            testSet.add(e);
        } 
        
        double testAccuracy = ann.predictionAccuracy(testSet);
        System.out.printf("Test Acc = %.3f\n", testAccuracy);
    }
    
    public static void testIrisWithFeatureSelection() throws InputAndWeightLengthMismatchExeption {
        Random rng = new Random(57492519);
        
        String trainFilename = "iris-train.txt";
        String testFilename = "iris-test.txt";
        
        final int numInputUnits = 4;
        
        List<Attribute> attrList = new ArrayList<Attribute>();
        int index = 0;
        
        // number of Hidden nodes attribute
        ContniousValueConvertor contValConvHidden = new ContniousValueConvertor(0, 6);  // 
        Attribute attrHidden = new Attribute(ANN_System.HIDDEN, new Range<Double>(1.0, true, 10.0, true), AttributeType.condition, index, 
                contValConvHidden, contValConvHidden.TOTAL_LEN) ;
        index += attrHidden.getLengthOfValueBits();
        attrList.add(attrHidden);
        
        // LEARNING_RATE attribute 
        ContniousValueConvertor contValConvLearningRate = new ContniousValueConvertor(4,14);
        Attribute attrLearningRate = new Attribute(ANN_System.LEARNING_RATE, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                contValConvLearningRate, contValConvLearningRate.TOTAL_LEN) ;
        index += attrLearningRate.getLengthOfValueBits();
        attrList.add(attrLearningRate);
        
        // MOMENTUM
        ContniousValueConvertor contValConvMomentum = new ContniousValueConvertor(4,14);
        Attribute attrMomentum = new Attribute(ANN_System.MOMENTUM, new Range<Double>(0.0 , true, 0.9, true), AttributeType.condition, index, 
                contValConvMomentum, contValConvMomentum.TOTAL_LEN) ;
        index += attrMomentum.getLengthOfValueBits();
        attrList.add(attrMomentum);
        
        // STOP_CRITERIA
        ContniousValueConvertor contValConvStopCriteria = new ContniousValueConvertor(0, 15);
        Attribute attrStopCriteria = new Attribute(ANN_System.STOP_CRITERIA, new Range<Double>(5000.0, true, 10000.0, true), AttributeType.condition, index, 
                contValConvStopCriteria, contValConvStopCriteria.TOTAL_LEN) ;
        index += attrStopCriteria.getLengthOfValueBits();
        attrList.add(attrStopCriteria);
        
        // add the input attrbutes to ANN. The values here will be used for feature selection
        for(int i = 0; i < numInputUnits; i++) {
            ContniousValueConvertor contValConvInputFeature = new ContniousValueConvertor(0,1);//(4,14);
            Attribute attrInputFeature = new Attribute(ANN_System.FEATURE_SELECTION, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                    contValConvInputFeature, contValConvInputFeature.TOTAL_LEN) ;
            index += attrInputFeature.getLengthOfValueBits();
            attrList.add(attrInputFeature);
        }
        
        List<String> targetValuesList = new ArrayList<String>();
        targetValuesList.add("Iris-setosa");
        targetValuesList.add("Iris-versicolor");
        targetValuesList.add("Iris-virginica");
        
        
        Scanner train;
        try {    
            train = new Scanner(new File(trainFilename));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> tmpExamples = new ArrayList<Example>();
        List<Example> trainingSet = new ArrayList<Example>();
        List<Example> validationSet = new ArrayList<Example>();
        System.out.println("Reading traing data...");
        
        while(train.hasNext()) {
            double[] input = new double[4];
            input[0] = train.nextDouble();
            input[1] = train.nextDouble();
            input[2] = train.nextDouble();
            input[3] = train.nextDouble();
            
            String tmpTarget = train.next();
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            tmpExamples.add(e);
            if(rng.nextDouble() < (1.0-VALIDATION_PROPORTION)) {
                trainingSet.add(e);
            } else {
                validationSet.add(e);
            }
        } 
        
        /////// A Neural Networks layers
        int layers = 3;
        int[] numUnitsInLayers = new int[layers];
        numUnitsInLayers[0] = numInputUnits;    // input layer
        // hidden layer will be set by the GA
        numUnitsInLayers[2] = targetValuesList.size();
        
        
        // Initialise Genetic Algorithm
        Fitness fitness = new FitnessAccuracySize(); 
        Double fitnessThreshold = null; // stop when fitnessThreshold of 90% or more has been achieved on training data.
        int maxIter = 10;
        Integer convergeAfterIter = null;//(int) (0.2 * maxIter);  // 20% of maxIteration 
        int p = 30;
        double r = 0.6; 
        double m = 0.01;
        int ruleLength = Rule.calculateRuleLength(attrList);
        int initialNumOfRules = 1;                          // Only need one rule.
        SelectionStrategy select = SelectionStrategy.Tornament;
        CrossoverType cross = CrossoverType.Uniform;
        
        GeneticAlgorithm ga = new GeneticAlgorithm(fitness, fitnessThreshold, p, r, m, ruleLength, 
                select, cross, attrList, initialNumOfRules, true, 
                maxIter, convergeAfterIter, numUnitsInLayers);
        
        
        // Learn using Genetic Algorithm
        Hypothesis learnedHypothesis = ga.learn(trainingSet, validationSet, targetValuesList);
        System.out.printf("Learned Hypothesis:\n%s\n", learnedHypothesis.toString());
        
        
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 10000;
        
        if(learnedHypothesis.getValueOf(ANN_System.HIDDEN) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.HIDDEN);
            numUnitsInLayers[1] = (int) tmp;
        }
        if(learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE) != null) {
            learningRate = (Double) learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE);
        }
        if(learnedHypothesis.getValueOf(ANN_System.MOMENTUM) != null) {
            momentum = (Double) learnedHypothesis.getValueOf(ANN_System.MOMENTUM);
        }
        if(learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA);
            stopCriteria = (int) tmp;
        }
        List<Object> featerList = learnedHypothesis.getValueListOf(ANN_System.FEATURE_SELECTION);
        if(featerList != null) {
            tmpExamples = Example.updateExampleInput(featerList, tmpExamples);
            validationSet = Example.updateExampleInput(featerList, validationSet);
            trainingSet = Example.updateExampleInput(featerList, trainingSet);
            if(tmpExamples.get(0) != null)
                numUnitsInLayers[0] = tmpExamples.get(0).getInput().length; 
        }
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(learnedHypothesis.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = learnedHypothesis.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn the ANN
        ann.learn(trainingSet, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        double trainingAccuracy = ann.predictionAccuracy(tmpExamples);
        
        System.out.println(ann.toString());
        
        System.out.printf("Validation Acc = %.3f\n", ann.predictionAccuracy(validationSet));
        System.out.printf("Train Acc = %.3f\n", trainingAccuracy);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // predict on test set
        Scanner test;
        try {
            test = new Scanner(new File(testFilename));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> testSet = new ArrayList<Example>();
        while(test.hasNext()) {
            double[] input = new double[4];
            input[0] = test.nextDouble();
            input[1] = test.nextDouble();
            input[2] = test.nextDouble();
            input[3] = test.nextDouble();
            
            String tmpTarget = test.next();
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            testSet.add(e);
        } 
        test.close();

        if(featerList != null) {    // extract only the selected features
            testSet = Example.updateExampleInput(featerList, testSet);
        }
        
        double testAccuracy = ann.predictionAccuracy(testSet);
        System.out.printf("Test Acc = %.3f\n", testAccuracy);
    }

    public static void testIrisWithWeightAndFeatureSelection() throws InputAndWeightLengthMismatchExeption {
        Random rng = new Random(57492519);
        
        String trainFilename = "iris-train.txt";
        String testFilename = "iris-test.txt";
        
        final int numInputUnits = 4;
        double maxHiddenUnits = 10.0;
        
        List<Attribute> attrList = new ArrayList<Attribute>();
        int index = 0;
        
        // number of Hidden nodes attribute
        ContniousValueConvertor contValConvHidden = new ContniousValueConvertor(0, 6);  // 
        Attribute attrHidden = new Attribute(ANN_System.HIDDEN, new Range<Double>(1.0, true, maxHiddenUnits, true), AttributeType.condition, index, 
                contValConvHidden, contValConvHidden.TOTAL_LEN) ;
        index += attrHidden.getLengthOfValueBits();
        attrList.add(attrHidden);
        
        // LEARNING_RATE attribute 
        ContniousValueConvertor contValConvLearningRate = new ContniousValueConvertor(4,14);
        Attribute attrLearningRate = new Attribute(ANN_System.LEARNING_RATE, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                contValConvLearningRate, contValConvLearningRate.TOTAL_LEN) ;
        index += attrLearningRate.getLengthOfValueBits();
        attrList.add(attrLearningRate);
        
        // MOMENTUM
        ContniousValueConvertor contValConvMomentum = new ContniousValueConvertor(4,14);
        Attribute attrMomentum = new Attribute(ANN_System.MOMENTUM, new Range<Double>(0.0 , true, 0.9, true), AttributeType.condition, index, 
                contValConvMomentum, contValConvMomentum.TOTAL_LEN) ;
        index += attrMomentum.getLengthOfValueBits();
        attrList.add(attrMomentum);
        
        // STOP_CRITERIA
        ContniousValueConvertor contValConvStopCriteria = new ContniousValueConvertor(0, 15);
        Attribute attrStopCriteria = new Attribute(ANN_System.STOP_CRITERIA, new Range<Double>(5000.0, true, 10000.0, true), AttributeType.condition, index, 
                contValConvStopCriteria, contValConvStopCriteria.TOTAL_LEN) ;
        index += attrStopCriteria.getLengthOfValueBits();
        attrList.add(attrStopCriteria);
        
        // add the input attrbutes to ANN. The values here will be used for feature selection
        for(int i = 0; i < numInputUnits; i++) {
            ContniousValueConvertor contValConvInputFeature = new ContniousValueConvertor(0,1);//(4,14);
            Attribute attrInputFeature = new Attribute(ANN_System.FEATURE_SELECTION, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                    contValConvInputFeature, contValConvInputFeature.TOTAL_LEN) ;
            index += attrInputFeature.getLengthOfValueBits();
            attrList.add(attrInputFeature);
        }
        
        List<String> targetValuesList = new ArrayList<String>();
        targetValuesList.add("Iris-setosa");
        targetValuesList.add("Iris-versicolor");
        targetValuesList.add("Iris-virginica");
        
        // initial weights attribute
        // weights attribute added last
        int maxNumInitialWeights = (int) (((numInputUnits + 1) * maxHiddenUnits) + ((maxHiddenUnits + 1) * targetValuesList.size()));
        for(int i = 0; i < maxNumInitialWeights; i++) {
            ContniousValueConvertor contValConvInitialWeights = new ContniousValueConvertor(4,16);
            Attribute attrInitialWeights = new Attribute(ANN_System.WEIGHT, new Range<Double>(-10.0 , true, 10.0, true), 
                    AttributeType.condition, index, contValConvInitialWeights, contValConvInitialWeights.TOTAL_LEN) ;
            index += attrInitialWeights.getLengthOfValueBits();
            attrList.add(attrInitialWeights);
        }
        
        Scanner train;
        try {    
            train = new Scanner(new File(trainFilename));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> tmpExamples = new ArrayList<Example>();
        List<Example> trainingSet = new ArrayList<Example>();
        List<Example> validationSet = new ArrayList<Example>();
        System.out.println("Reading traing data...");
        
        while(train.hasNext()) {
            double[] input = new double[4];
            input[0] = train.nextDouble();
            input[1] = train.nextDouble();
            input[2] = train.nextDouble();
            input[3] = train.nextDouble();
            
            String tmpTarget = train.next();
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            tmpExamples.add(e);
            if(rng.nextDouble() < (1.0-VALIDATION_PROPORTION)) {
                trainingSet.add(e);
            } else {
                validationSet.add(e);
            }
        } 
        
        /////// A Neural Networks layers
        int layers = 3;
        int[] numUnitsInLayers = new int[layers];
        numUnitsInLayers[0] = numInputUnits;    // input layer
        // hidden layer will be set by the GA
        numUnitsInLayers[2] = targetValuesList.size();
        
        
        // Initialise Genetic Algorithm
        Fitness fitness = new FitnessAccuracySize(); 
        Double fitnessThreshold = null; // stop when fitnessThreshold of 90% or more has been achieved on training data.
        int maxIter = 10;
        Integer convergeAfterIter = null;//(int) (0.2 * maxIter);  // 20% of maxIteration 
        int p = 30;
        double r = 0.6; 
        double m = 0.01;
        int ruleLength = Rule.calculateRuleLength(attrList);
        int initialNumOfRules = 1;                          // Only need one rule.
        SelectionStrategy select = SelectionStrategy.Tornament;
        CrossoverType cross = CrossoverType.Uniform;
        
        GeneticAlgorithm ga = new GeneticAlgorithm(fitness, fitnessThreshold, p, r, m, ruleLength, 
                select, cross, attrList, initialNumOfRules, true, 
                maxIter, convergeAfterIter, numUnitsInLayers);
        
        
        // Learn using Genetic Algorithm
        Hypothesis learnedHypothesis = ga.learn(trainingSet, validationSet, targetValuesList);
        System.out.printf("Learned Hypothesis:\n%s\n", learnedHypothesis.toString());
        
        
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 10000;
        
        if(learnedHypothesis.getValueOf(ANN_System.HIDDEN) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.HIDDEN);
            numUnitsInLayers[1] = (int) tmp;
        }
        if(learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE) != null) {
            learningRate = (Double) learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE);
        }
        if(learnedHypothesis.getValueOf(ANN_System.MOMENTUM) != null) {
            momentum = (Double) learnedHypothesis.getValueOf(ANN_System.MOMENTUM);
        }
        if(learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA);
            stopCriteria = (int) tmp;
        }
        List<Object> featerList = learnedHypothesis.getValueListOf(ANN_System.FEATURE_SELECTION);
        if(featerList != null) {
            tmpExamples = Example.updateExampleInput(featerList, tmpExamples);
            validationSet = Example.updateExampleInput(featerList, validationSet);
            trainingSet = Example.updateExampleInput(featerList, trainingSet);
            if(tmpExamples.get(0) != null)
                numUnitsInLayers[0] = tmpExamples.get(0).getInput().length; 
        }
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(learnedHypothesis.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = learnedHypothesis.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn the ANN
        ann.learn(trainingSet, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        double trainingAccuracy = ann.predictionAccuracy(tmpExamples);
        
        System.out.println(ann.toString());
        
        System.out.printf("Validation Acc = %.3f\n", ann.predictionAccuracy(validationSet));
        System.out.printf("Train Acc = %.3f\n", trainingAccuracy);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // predict on test set
        Scanner test;
        try {
            test = new Scanner(new File(testFilename));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> testSet = new ArrayList<Example>();
        while(test.hasNext()) {
            double[] input = new double[4];
            input[0] = test.nextDouble();
            input[1] = test.nextDouble();
            input[2] = test.nextDouble();
            input[3] = test.nextDouble();
            
            String tmpTarget = test.next();
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            testSet.add(e);
        } 
        test.close();

        if(featerList != null) {    // extract only the selected features
            testSet = Example.updateExampleInput(featerList, testSet);
        }
        
        double testAccuracy = ann.predictionAccuracy(testSet);
        System.out.printf("Test Acc = %.3f\n", testAccuracy);
    }
    
    public static void testIrisPrevPaper() throws InputAndWeightLengthMismatchExeption {
        Random rng = new Random(57492519);
        
        String trainFilename = "iris-train.txt";
        String testFilename = "iris-test.txt";
        
        int numInputUnits = 4;
        double maxHiddenUnits = 10.0;
        
        List<Attribute> attrList = new ArrayList<Attribute>();
        int index = 0;
        
        // number of Hidden nodes attribute
        ContniousValueConvertor contValConvHidden = new ContniousValueConvertor(0, 6);  //
        Attribute attrHidden = new Attribute(ANN_System.HIDDEN, new Range<Double>(1.0, true, maxHiddenUnits, true), AttributeType.condition, index, 
                contValConvHidden, contValConvHidden.TOTAL_LEN) ;
        index += attrHidden.getLengthOfValueBits();
        attrList.add(attrHidden);
        
        
        List<String> targetValuesList = new ArrayList<String>();
        targetValuesList.add("Iris-setosa");
        targetValuesList.add("Iris-versicolor");
        targetValuesList.add("Iris-virginica");
        
        // initial weights attribute
        // weights attribute added last
        int maxNumInitialWeights = (int) (((numInputUnits + 1) * maxHiddenUnits) + ((maxHiddenUnits + 1) * targetValuesList.size()));
        for(int i = 0; i < maxNumInitialWeights; i++) {
            ContniousValueConvertor contValConvInitialWeights = new ContniousValueConvertor(4,16);
            Attribute attrInitialWeights = new Attribute(ANN_System.WEIGHT, new Range<Double>(-10.0 , true, 10.0, true), 
                    AttributeType.condition, index, contValConvInitialWeights, contValConvInitialWeights.TOTAL_LEN) ;
            index += attrInitialWeights.getLengthOfValueBits();
            attrList.add(attrInitialWeights);
        }
        
        
        Scanner train;
        try {    
            train = new Scanner(new File(trainFilename));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> tmpExamples = new ArrayList<Example>();
        List<Example> trainingSet = new ArrayList<Example>();
        List<Example> validationSet = new ArrayList<Example>();
        System.out.println("Reading traing data...");
        
        while(train.hasNext()) {
            double[] input = new double[numInputUnits];
            input[0] = train.nextDouble();
            input[1] = train.nextDouble();
            input[2] = train.nextDouble();
            input[3] = train.nextDouble();
            
            String tmpTarget = train.next();
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            tmpExamples.add(e);
            if(rng.nextDouble() < (1.0-VALIDATION_PROPORTION)) {
                trainingSet.add(e);
            } else {
                validationSet.add(e);
            }
        } 
        
        /////// A Neural Networks layers
        int layers = 3;
        int[] numUnitsInLayers = new int[layers];
        numUnitsInLayers[0] = numInputUnits;    // input layer
        // hidden layer will be set by the GA
        numUnitsInLayers[2] = targetValuesList.size();
        
        
        // Initialise Genetic Algorithm
        Fitness fitness = new FitnessAccuracySize(); 
        Double fitnessThreshold = null; // stop when fitnessThreshold of 90% or more has been achieved on training data.
        int maxIter = 10;
        Integer convergeAfterIter = null;//(int) (0.2 * maxIter);  // 20% of maxIteration 
        int p = 30;
        double r = 0.6; 
        double m = 0.01;
        int ruleLength = Rule.calculateRuleLength(attrList);
        int initialNumOfRules = 1;                          // Only need one rule.
        SelectionStrategy select = SelectionStrategy.Tornament;
        CrossoverType cross = CrossoverType.Uniform;
        
        GeneticAlgorithm ga = new GeneticAlgorithm(fitness, fitnessThreshold, p, r, m, ruleLength, 
                select, cross, attrList, initialNumOfRules, true, 
                maxIter, convergeAfterIter, numUnitsInLayers);
        
        
        // Learn using Genetic Algorithm
        Hypothesis learnedHypothesis = ga.learn(trainingSet, validationSet, targetValuesList);
        System.out.printf("Learned Hypothesis:\n%s\n", learnedHypothesis.toString());
        
        
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 10000;
        
        if(learnedHypothesis.getValueOf(ANN_System.HIDDEN) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.HIDDEN);
            numUnitsInLayers[1] = (int) tmp;
        }
        if(learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE) != null) {
            learningRate = (Double) learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE);
        }
        if(learnedHypothesis.getValueOf(ANN_System.MOMENTUM) != null) {
            momentum = (Double) learnedHypothesis.getValueOf(ANN_System.MOMENTUM);
        }
        if(learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA);
            stopCriteria = (int) tmp;
        }
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(learnedHypothesis.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = learnedHypothesis.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn the ANN
        ann.learn(trainingSet, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        double trainingAccuracy = ann.predictionAccuracy(tmpExamples);
        
        System.out.println(ann.toString());
        
        System.out.printf("Validation Acc = %.3f\n", ann.predictionAccuracy(validationSet));
        System.out.printf("Train Acc = %.3f\n", trainingAccuracy);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // predict on test set
        Scanner test;
        try {
            test = new Scanner(new File(testFilename));
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> testSet = new ArrayList<Example>();
        while(test.hasNext()) {
            double[] input = new double[numInputUnits];
            input[0] = test.nextDouble();
            input[1] = test.nextDouble();
            input[2] = test.nextDouble();
            input[3] = test.nextDouble();
            
            String tmpTarget = test.next();
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            testSet.add(e);
        } 
        
        double testAccuracy = ann.predictionAccuracy(testSet);
        System.out.printf("Test Acc = %.3f\n", testAccuracy);
    }
    
    public static void testBreastCancer() throws InputAndWeightLengthMismatchExeption {
        Random rng = new Random(57492519);
        
        String trainFilename = "bcancer.train";
        String testFilename = "bcancer.test";
        
        // The input size of ANN
        final int ANN_INPUT_SIZE = 30;
        
        List<Attribute> attrList = new ArrayList<Attribute>();
        int index = 0;
        
        // number of Hidden nodes attribute
        ContniousValueConvertor contValConvHidden = new ContniousValueConvertor(0, 6);  // 
        Attribute attrHidden = new Attribute(ANN_System.HIDDEN, new Range<Double>(1.0, true, 20.0, true), AttributeType.condition, index, 
                contValConvHidden, contValConvHidden.TOTAL_LEN) ;
        index += attrHidden.getLengthOfValueBits();
        attrList.add(attrHidden);
        
        // LEARNING_RATE attribute 
        ContniousValueConvertor contValConvLearningRate = new ContniousValueConvertor(4,14);
        Attribute attrLearningRate = new Attribute(ANN_System.LEARNING_RATE, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                contValConvLearningRate, contValConvLearningRate.TOTAL_LEN) ;
        index += attrLearningRate.getLengthOfValueBits();
        attrList.add(attrLearningRate);
        
        // MOMENTUM
        ContniousValueConvertor contValConvMomentum = new ContniousValueConvertor(4,14);
        Attribute attrMomentum = new Attribute(ANN_System.MOMENTUM, new Range<Double>(0.0 , true, 0.9, true), AttributeType.condition, index, 
                contValConvMomentum, contValConvMomentum.TOTAL_LEN) ;
        index += attrMomentum.getLengthOfValueBits();
        attrList.add(attrMomentum);
        
        // STOP_CRITERIA
        ContniousValueConvertor contValConvStopCriteria = new ContniousValueConvertor(0, 15);
        Attribute attrStopCriteria = new Attribute(ANN_System.STOP_CRITERIA, new Range<Double>(1000.0, true, 10000.0, true), AttributeType.condition, index, 
                contValConvStopCriteria, contValConvStopCriteria.TOTAL_LEN) ;
        index += attrStopCriteria.getLengthOfValueBits();
        attrList.add(attrStopCriteria);
        
        List<String> targetValuesList = new ArrayList<String>();
        targetValuesList.add("B");
        targetValuesList.add("M");
        
        
        Scanner train;
        try {    
            train = new Scanner(new File(trainFilename));
            train.useDelimiter("[,\\s]+");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> tmpExamples = new ArrayList<Example>();
        List<Example> trainingSet = new ArrayList<Example>();
        List<Example> validationSet = new ArrayList<Example>();
        System.out.println("Reading traing data...");
        
        
        while(train.hasNext()) {
            double[] input = new double[ANN_INPUT_SIZE];
            int id = train.nextInt();
            String tmpTarget = train.next();
            
            for(int i = 0; i < input.length; i++) {
                input[i] = train.nextDouble();
            }
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            tmpExamples.add(e);
            if(rng.nextDouble() < (1.0-VALIDATION_PROPORTION)) {
                trainingSet.add(e);
            } else {
                validationSet.add(e);
            }
        } 
        train.close();
        
        /////// A Neural Networks layers
        int layers = 3;
        int[] numUnitsInLayers = new int[layers];
        numUnitsInLayers[0] = ANN_INPUT_SIZE;    // input layer
        // hidden layer will be set by the GA
        numUnitsInLayers[2] = targetValuesList.size();
        
        
        // Initialise Genetic Algorithm
        Fitness fitness = new FitnessAccuracySize(); 
        Double fitnessThreshold = null; // stop when fitnessThreshold of 90% or more has been achieved on training data.
        int maxIter = 10;
        Integer convergeAfterIter = null;//(int) (0.2 * maxIter);  // 20% of maxIteration 
        int p = 10;
        double r = 0.6; 
        double m = 0.01;
        int ruleLength = Rule.calculateRuleLength(attrList);
        int initialNumOfRules = 1;                          // Only need one rule.
        SelectionStrategy select = SelectionStrategy.Tornament;
        CrossoverType cross = CrossoverType.Uniform;
        
        GeneticAlgorithm ga = new GeneticAlgorithm(fitness, fitnessThreshold, p, r, m, ruleLength, 
                select, cross, attrList, initialNumOfRules, true, 
                maxIter, convergeAfterIter, numUnitsInLayers);
        
        
        // Learn using Genetic Algorithm
        Hypothesis learnedHypothesis = ga.learn(trainingSet, validationSet, targetValuesList);
        System.out.printf("Learned Hypothesis:\n%s\n", learnedHypothesis.toString());
        
        
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 10000;
        
        if(learnedHypothesis.getValueOf(ANN_System.HIDDEN) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.HIDDEN);
            numUnitsInLayers[1] = (int) tmp;
        }
        if(learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE) != null) {
            learningRate = (Double) learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE);
        }
        if(learnedHypothesis.getValueOf(ANN_System.MOMENTUM) != null) {
            momentum = (Double) learnedHypothesis.getValueOf(ANN_System.MOMENTUM);
        }
        if(learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA);
            stopCriteria = (int) tmp;
        }
        
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(learnedHypothesis.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = learnedHypothesis.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn the ANN
        ann.learn(trainingSet, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        double trainingAccuracy = ann.predictionAccuracy(tmpExamples);
        
        System.out.println(ann.toString());
        
        System.out.printf("Validation Acc = %.3f\n", ann.predictionAccuracy(validationSet));
        System.out.printf("Train Acc = %.3f\n", trainingAccuracy);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // predict on test set
        Scanner test;
        try {
            test = new Scanner(new File(testFilename));
            test.useDelimiter("[,\\s]+");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> testSet = new ArrayList<Example>();
        while(test.hasNext()) {
            double[] input = new double[ANN_INPUT_SIZE];
            int id = test.nextInt();
            String tmpTarget = test.next();
            
            for(int i = 0; i < input.length; i++) {
                input[i] = test.nextDouble();
            }
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            testSet.add(e);
        } 
        test.close();
        
        double testAccuracy = ann.predictionAccuracy(testSet);
        System.out.printf("Test Acc = %.3f\n", testAccuracy);
    }
    
    
    public static void testBreastCancerWithFeatureSelection() throws InputAndWeightLengthMismatchExeption {
        Random rng = new Random(57492519);
        
        String trainFilename = "bcancer.train";
        String testFilename = "bcancer.test";
        
        // The input size of ANN
        final int ANN_INPUT_SIZE = 30;
        
        List<Attribute> attrList = new ArrayList<Attribute>();
        int index = 0;
        
        // number of Hidden nodes attribute
        ContniousValueConvertor contValConvHidden = new ContniousValueConvertor(0, 6);  // 
        Attribute attrHidden = new Attribute(ANN_System.HIDDEN, new Range<Double>(1.0, true, 20.0, true), AttributeType.condition, index, 
                contValConvHidden, contValConvHidden.TOTAL_LEN) ;
        index += attrHidden.getLengthOfValueBits();
        attrList.add(attrHidden);
        
        // LEARNING_RATE attribute 
        ContniousValueConvertor contValConvLearningRate = new ContniousValueConvertor(4,14);
        Attribute attrLearningRate = new Attribute(ANN_System.LEARNING_RATE, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                contValConvLearningRate, contValConvLearningRate.TOTAL_LEN) ;
        index += attrLearningRate.getLengthOfValueBits();
        attrList.add(attrLearningRate);
        
        // MOMENTUM
        ContniousValueConvertor contValConvMomentum = new ContniousValueConvertor(4,14);
        Attribute attrMomentum = new Attribute(ANN_System.MOMENTUM, new Range<Double>(0.0 , true, 0.9, true), AttributeType.condition, index, 
                contValConvMomentum, contValConvMomentum.TOTAL_LEN) ;
        index += attrMomentum.getLengthOfValueBits();
        attrList.add(attrMomentum);
        
        // STOP_CRITERIA
        ContniousValueConvertor contValConvStopCriteria = new ContniousValueConvertor(0, 15);
        Attribute attrStopCriteria = new Attribute(ANN_System.STOP_CRITERIA, new Range<Double>(1000.0, true, 10000.0, true), AttributeType.condition, index, 
                contValConvStopCriteria, contValConvStopCriteria.TOTAL_LEN) ;
        index += attrStopCriteria.getLengthOfValueBits();
        attrList.add(attrStopCriteria);
        
        // add the input attrbutes to ANN. The values here will be used for feature selection
        for(int i = 0; i < ANN_INPUT_SIZE; i++) {
            ContniousValueConvertor contValConvInputFeature = new ContniousValueConvertor(4,14);
            Attribute attrInputFeature = new Attribute(ANN_System.FEATURE_SELECTION, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                    contValConvInputFeature, contValConvInputFeature.TOTAL_LEN) ;
            index += attrInputFeature.getLengthOfValueBits();
            attrList.add(attrInputFeature);
        }
        
        
        List<String> targetValuesList = new ArrayList<String>();
        targetValuesList.add("B");
        targetValuesList.add("M");
        
        
        Scanner train;
        try {    
            train = new Scanner(new File(trainFilename));
            train.useDelimiter("[,\\s]+");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> tmpExamples = new ArrayList<Example>();
        List<Example> trainingSet = new ArrayList<Example>();
        List<Example> validationSet = new ArrayList<Example>();
        System.out.println("Reading traing data...");
        
        
        while(train.hasNext()) {
            double[] input = new double[ANN_INPUT_SIZE];
            int id = train.nextInt();
            String tmpTarget = train.next();
            
            for(int i = 0; i < input.length; i++) {
                input[i] = train.nextDouble();
            }
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            tmpExamples.add(e);
            if(rng.nextDouble() < (1.0-VALIDATION_PROPORTION)) {
                trainingSet.add(e);
            } else {
                validationSet.add(e);
            }
        } 
        train.close();
        
        /////// A Neural Networks layers
        int layers = 3;
        int[] numUnitsInLayers = new int[layers];
        numUnitsInLayers[0] = ANN_INPUT_SIZE;    // input layer
        // hidden layer will be set by the GA
        numUnitsInLayers[2] = targetValuesList.size();
        
        
        // Initialise Genetic Algorithm
        Fitness fitness = new FitnessAccuracySize(); 
        Double fitnessThreshold = null; // stop when fitnessThreshold of 90% or more has been achieved on training data.
        int maxIter = 10;
        Integer convergeAfterIter = null;//(int) (0.2 * maxIter);  // 20% of maxIteration 
        int p = 10;
        double r = 0.6; 
        double m = 0.01;
        int ruleLength = Rule.calculateRuleLength(attrList);
        int initialNumOfRules = 1;                          // Only need one rule.
        SelectionStrategy select = SelectionStrategy.Tornament;
        CrossoverType cross = CrossoverType.Uniform;
        
        GeneticAlgorithm ga = new GeneticAlgorithm(fitness, fitnessThreshold, p, r, m, ruleLength, 
                select, cross, attrList, initialNumOfRules, true, 
                maxIter, convergeAfterIter, numUnitsInLayers);
        
        
        // Learn using Genetic Algorithm
        Hypothesis learnedHypothesis = ga.learn(trainingSet, validationSet, targetValuesList);
        System.out.printf("Learned Hypothesis:\n%s\n", learnedHypothesis.toString());
        
        
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 10000;
        
        if(learnedHypothesis.getValueOf(ANN_System.HIDDEN) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.HIDDEN);
            numUnitsInLayers[1] = (int) tmp;
        }
        if(learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE) != null) {
            learningRate = (Double) learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE);
        }
        if(learnedHypothesis.getValueOf(ANN_System.MOMENTUM) != null) {
            momentum = (Double) learnedHypothesis.getValueOf(ANN_System.MOMENTUM);
        }
        if(learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA);
            stopCriteria = (int) tmp;
        }
        List<Object> featerList = learnedHypothesis.getValueListOf(ANN_System.FEATURE_SELECTION);
        if(featerList != null) {
            tmpExamples = Example.updateExampleInput(featerList, tmpExamples);
            validationSet = Example.updateExampleInput(featerList, validationSet);
            trainingSet = Example.updateExampleInput(featerList, trainingSet);
            if(tmpExamples.get(0) != null)
                numUnitsInLayers[0] = tmpExamples.get(0).getInput().length; 
        }
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(learnedHypothesis.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = learnedHypothesis.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn the ANN
        ann.learn(tmpExamples, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        double trainingAccuracy = ann.predictionAccuracy(tmpExamples);
        
        
        System.out.println(ann.toString());
        
        System.out.printf("Validation Acc = %.3f\n", ann.predictionAccuracy(validationSet));
        System.out.printf("Train Acc = %.3f\n", trainingAccuracy);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // predict on test set
        Scanner test;
        try {
            test = new Scanner(new File(testFilename));
            test.useDelimiter("[,\\s]+");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> testSet = new ArrayList<Example>();
        while(test.hasNext()) {
            double[] input = new double[ANN_INPUT_SIZE];
            int id = test.nextInt();
            String tmpTarget = test.next();
            
            for(int i = 0; i < input.length; i++) {
                input[i] = test.nextDouble();
            }
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            testSet.add(e);
        } 
        test.close();
        
        if(featerList != null) {    // extract only the selected features
            testSet = Example.updateExampleInput(featerList, testSet);
        }
        
        double testAccuracy = ann.predictionAccuracy(testSet);
        System.out.printf("Test Acc = %.3f\n", testAccuracy);
    }
    
    
    public static void testBreastCancerWithWeightAndFeatureSelection() throws InputAndWeightLengthMismatchExeption {
        Random rng = new Random(57492519);
        
        String trainFilename = "bcancer.train";
        String testFilename = "bcancer.test";
        
        // The input size of ANN
        final int numInputUnits = 30;
        final double maxHiddenUnits = 20.0;
        
        List<Attribute> attrList = new ArrayList<Attribute>();
        int index = 0;
        
        // number of Hidden nodes attribute
        ContniousValueConvertor contValConvHidden = new ContniousValueConvertor(0, 6);  // 
        Attribute attrHidden = new Attribute(ANN_System.HIDDEN, new Range<Double>(1.0, true, maxHiddenUnits, true), AttributeType.condition, index, 
                contValConvHidden, contValConvHidden.TOTAL_LEN) ;
        index += attrHidden.getLengthOfValueBits();
        attrList.add(attrHidden);
        
        // LEARNING_RATE attribute 
        ContniousValueConvertor contValConvLearningRate = new ContniousValueConvertor(4,14);
        Attribute attrLearningRate = new Attribute(ANN_System.LEARNING_RATE, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                contValConvLearningRate, contValConvLearningRate.TOTAL_LEN) ;
        index += attrLearningRate.getLengthOfValueBits();
        attrList.add(attrLearningRate);
        
        // MOMENTUM
        ContniousValueConvertor contValConvMomentum = new ContniousValueConvertor(4,14);
        Attribute attrMomentum = new Attribute(ANN_System.MOMENTUM, new Range<Double>(0.0 , true, 0.9, true), AttributeType.condition, index, 
                contValConvMomentum, contValConvMomentum.TOTAL_LEN) ;
        index += attrMomentum.getLengthOfValueBits();
        attrList.add(attrMomentum);
        
        // STOP_CRITERIA
        ContniousValueConvertor contValConvStopCriteria = new ContniousValueConvertor(0, 15);
        Attribute attrStopCriteria = new Attribute(ANN_System.STOP_CRITERIA, new Range<Double>(1000.0, true, 10000.0, true), AttributeType.condition, index, 
                contValConvStopCriteria, contValConvStopCriteria.TOTAL_LEN) ;
        index += attrStopCriteria.getLengthOfValueBits();
        attrList.add(attrStopCriteria);
        
        // add the input attrbutes to ANN. The values here will be used for feature selection
        for(int i = 0; i < numInputUnits; i++) {
            ContniousValueConvertor contValConvInputFeature = new ContniousValueConvertor(4,14);
            Attribute attrInputFeature = new Attribute(ANN_System.FEATURE_SELECTION, new Range<Double>(0.0 , true, 1.0, true), AttributeType.condition, index, 
                    contValConvInputFeature, contValConvInputFeature.TOTAL_LEN) ;
            index += attrInputFeature.getLengthOfValueBits();
            attrList.add(attrInputFeature);
        }
        
        
        List<String> targetValuesList = new ArrayList<String>();
        targetValuesList.add("B");
        targetValuesList.add("M");
        
        // initial weights attribute
        // weights attribute added last
        int maxNumInitialWeights = (int) (((numInputUnits + 1) * maxHiddenUnits) + ((maxHiddenUnits + 1) * targetValuesList.size()));
        for(int i = 0; i < maxNumInitialWeights; i++) {
            ContniousValueConvertor contValConvInitialWeights = new ContniousValueConvertor(4,16);
            Attribute attrInitialWeights = new Attribute(ANN_System.WEIGHT, new Range<Double>(-10.0 , true, 10.0, true), 
                    AttributeType.condition, index, contValConvInitialWeights, contValConvInitialWeights.TOTAL_LEN) ;
            index += attrInitialWeights.getLengthOfValueBits();
            attrList.add(attrInitialWeights);
        }
        
        Scanner train;
        try {    
            train = new Scanner(new File(trainFilename));
            train.useDelimiter("[,\\s]+");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> tmpExamples = new ArrayList<Example>();
        List<Example> trainingSet = new ArrayList<Example>();
        List<Example> validationSet = new ArrayList<Example>();
        System.out.println("Reading traing data...");
        
        
        while(train.hasNext()) {
            double[] input = new double[numInputUnits];
            int id = train.nextInt();
            String tmpTarget = train.next();
            
            for(int i = 0; i < input.length; i++) {
                input[i] = train.nextDouble();
            }
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            tmpExamples.add(e);
            if(rng.nextDouble() < (1.0-VALIDATION_PROPORTION)) {
                trainingSet.add(e);
            } else {
                validationSet.add(e);
            }
        } 
        train.close();
        
        /////// A Neural Networks layers
        int layers = 3;
        int[] numUnitsInLayers = new int[layers];
        numUnitsInLayers[0] = numInputUnits;    // input layer
        // hidden layer will be set by the GA
        numUnitsInLayers[2] = targetValuesList.size();
        
        
        // Initialise Genetic Algorithm
        Fitness fitness = new FitnessAccuracySize(); 
        Double fitnessThreshold = null; // stop when fitnessThreshold of 90% or more has been achieved on training data.
        int maxIter = 10;
        Integer convergeAfterIter = null;//(int) (0.2 * maxIter);  // 20% of maxIteration 
        int p = 10;
        double r = 0.6; 
        double m = 0.01;
        int ruleLength = Rule.calculateRuleLength(attrList);
        int initialNumOfRules = 1;                          // Only need one rule.
        SelectionStrategy select = SelectionStrategy.Rank;
        CrossoverType cross = CrossoverType.Uniform;
        
        GeneticAlgorithm ga = new GeneticAlgorithm(fitness, fitnessThreshold, p, r, m, ruleLength, 
                select, cross, attrList, initialNumOfRules, true, 
                maxIter, convergeAfterIter, numUnitsInLayers);
        
        
        // Learn using Genetic Algorithm
        Hypothesis learnedHypothesis = ga.learn(trainingSet, validationSet, targetValuesList);
        System.out.printf("Learned Hypothesis:\n%s\n", learnedHypothesis.toString());
        
        
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 10000;
        
        if(learnedHypothesis.getValueOf(ANN_System.HIDDEN) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.HIDDEN);
            numUnitsInLayers[1] = (int) tmp;
        }
        if(learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE) != null) {
            learningRate = (Double) learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE);
        }
        if(learnedHypothesis.getValueOf(ANN_System.MOMENTUM) != null) {
            momentum = (Double) learnedHypothesis.getValueOf(ANN_System.MOMENTUM);
        }
        if(learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA);
            stopCriteria = (int) tmp;
        }
        List<Object> featerList = learnedHypothesis.getValueListOf(ANN_System.FEATURE_SELECTION);
        if(featerList != null) {
            tmpExamples = Example.updateExampleInput(featerList, tmpExamples);
            validationSet = Example.updateExampleInput(featerList, validationSet);
            trainingSet = Example.updateExampleInput(featerList, trainingSet);
            if(tmpExamples.get(0) != null)
                numUnitsInLayers[0] = tmpExamples.get(0).getInput().length; 
        }
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(learnedHypothesis.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = learnedHypothesis.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn the ANN
        ann.learn(tmpExamples, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        double trainingAccuracy = ann.predictionAccuracy(tmpExamples);
        
        
        System.out.println(ann.toString());
        
        System.out.printf("Validation Acc = %.3f\n", ann.predictionAccuracy(validationSet));
        System.out.printf("Train Acc = %.3f\n", trainingAccuracy);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // predict on test set
        Scanner test;
        try {
            test = new Scanner(new File(testFilename));
            test.useDelimiter("[,\\s]+");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> testSet = new ArrayList<Example>();
        while(test.hasNext()) {
            double[] input = new double[numInputUnits];
            int id = test.nextInt();
            String tmpTarget = test.next();
            
            for(int i = 0; i < input.length; i++) {
                input[i] = test.nextDouble();
            }
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            testSet.add(e);
        } 
        test.close();
        
        if(featerList != null) {    // extract only the selected features
            testSet = Example.updateExampleInput(featerList, testSet);
        }
        
        double testAccuracy = ann.predictionAccuracy(testSet);
        System.out.printf("Test Acc = %.3f\n", testAccuracy);
    }
    
    public static void testBreastCancerPrevPaper() throws InputAndWeightLengthMismatchExeption {
        Random rng = new Random(57492519);
        
        String trainFilename = "bcancer.train";
        String testFilename = "bcancer.test";
        
        // The input size of ANN
        final int numInputUnits = 30;
        final double maxHiddenUnits = 20.0;
        
        List<Attribute> attrList = new ArrayList<Attribute>();
        int index = 0;
        
        // number of Hidden nodes attribute
        ContniousValueConvertor contValConvHidden = new ContniousValueConvertor(0, 6);  // 
        Attribute attrHidden = new Attribute(ANN_System.HIDDEN, new Range<Double>(1.0, true, maxHiddenUnits, true), AttributeType.condition, index, 
                contValConvHidden, contValConvHidden.TOTAL_LEN) ;
        index += attrHidden.getLengthOfValueBits();
        attrList.add(attrHidden);
        
        List<String> targetValuesList = new ArrayList<String>();
        targetValuesList.add("B");
        targetValuesList.add("M");
        
        // initial weights attribute
        // weights attribute added last
        int maxNumInitialWeights = (int) (((numInputUnits + 1) * maxHiddenUnits) + ((maxHiddenUnits + 1) * targetValuesList.size()));
        for(int i = 0; i < maxNumInitialWeights; i++) {
            ContniousValueConvertor contValConvInitialWeights = new ContniousValueConvertor(4,16);
            Attribute attrInitialWeights = new Attribute(ANN_System.WEIGHT, new Range<Double>(-10.0 , true, 10.0, true), 
                    AttributeType.condition, index, contValConvInitialWeights, contValConvInitialWeights.TOTAL_LEN) ;
            index += attrInitialWeights.getLengthOfValueBits();
            attrList.add(attrInitialWeights);
        }
        
        Scanner train;
        try {    
            train = new Scanner(new File(trainFilename));
            train.useDelimiter("[,\\s]+");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> tmpExamples = new ArrayList<Example>();
        List<Example> trainingSet = new ArrayList<Example>();
        List<Example> validationSet = new ArrayList<Example>();
        System.out.println("Reading traing data...");
        
        
        while(train.hasNext()) {
            double[] input = new double[numInputUnits];
            int id = train.nextInt();
            String tmpTarget = train.next();
            
            for(int i = 0; i < input.length; i++) {
                input[i] = train.nextDouble();
            }
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            tmpExamples.add(e);
            if(rng.nextDouble() < (1.0-VALIDATION_PROPORTION)) {
                trainingSet.add(e);
            } else {
                validationSet.add(e);
            }
        } 
        train.close();
        
        /////// A Neural Networks layers
        int layers = 3;
        int[] numUnitsInLayers = new int[layers];
        numUnitsInLayers[0] = numInputUnits;    // input layer
        // hidden layer will be set by the GA
        numUnitsInLayers[2] = targetValuesList.size();
        
        
        // Initialise Genetic Algorithm
        Fitness fitness = new FitnessAccuracySize(); 
        Double fitnessThreshold = null; // stop when fitnessThreshold of 90% or more has been achieved on training data.
        int maxIter = 10;
        Integer convergeAfterIter = null;//(int) (0.2 * maxIter);  // 20% of maxIteration 
        int p = 10;
        double r = 0.6; 
        double m = 0.01;
        int ruleLength = Rule.calculateRuleLength(attrList);
        int initialNumOfRules = 1;                          // Only need one rule.
        SelectionStrategy select = SelectionStrategy.Rank;
        CrossoverType cross = CrossoverType.Uniform;
        
        GeneticAlgorithm ga = new GeneticAlgorithm(fitness, fitnessThreshold, p, r, m, ruleLength, 
                select, cross, attrList, initialNumOfRules, true, 
                maxIter, convergeAfterIter, numUnitsInLayers);
        
        
        // Learn using Genetic Algorithm
        Hypothesis learnedHypothesis = ga.learn(trainingSet, validationSet, targetValuesList);
        System.out.printf("Learned Hypothesis:\n%s\n", learnedHypothesis.toString());
        
        
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 7000;
        
        if(learnedHypothesis.getValueOf(ANN_System.HIDDEN) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.HIDDEN);
            numUnitsInLayers[1] = (int) tmp;
        }
        if(learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE) != null) {
            learningRate = (Double) learnedHypothesis.getValueOf(ANN_System.LEARNING_RATE);
        }
        if(learnedHypothesis.getValueOf(ANN_System.MOMENTUM) != null) {
            momentum = (Double) learnedHypothesis.getValueOf(ANN_System.MOMENTUM);
        }
        if(learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA) != null) {
            double tmp = (Double) learnedHypothesis.getValueOf(ANN_System.STOP_CRITERIA);
            stopCriteria = (int) tmp;
        }
        List<Object> featerList = learnedHypothesis.getValueListOf(ANN_System.FEATURE_SELECTION);
        if(featerList != null) {
            tmpExamples = Example.updateExampleInput(featerList, tmpExamples);
            validationSet = Example.updateExampleInput(featerList, validationSet);
            trainingSet = Example.updateExampleInput(featerList, trainingSet);
            if(tmpExamples.get(0) != null)
                numUnitsInLayers[0] = tmpExamples.get(0).getInput().length; 
        }
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(learnedHypothesis.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = learnedHypothesis.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn the ANN
        ann.learn(tmpExamples, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        double trainingAccuracy = ann.predictionAccuracy(tmpExamples);
        
        
        System.out.println(ann.toString());
        
        System.out.printf("Validation Acc = %.3f\n", ann.predictionAccuracy(validationSet));
        System.out.printf("Train Acc = %.3f\n", trainingAccuracy);
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////
        // predict on test set
        Scanner test;
        try {
            test = new Scanner(new File(testFilename));
            test.useDelimiter("[,\\s]+");
        } catch (FileNotFoundException e) {
            System.err.println("ERROR: Training file not found!");
            return;
        }
        
        List<Example> testSet = new ArrayList<Example>();
        while(test.hasNext()) {
            double[] input = new double[numInputUnits];
            int id = test.nextInt();
            String tmpTarget = test.next();
            
            for(int i = 0; i < input.length; i++) {
                input[i] = test.nextDouble();
            }
            
            double[] output = DiscreteInputOutput.conver1ToN(targetValuesList, tmpTarget);
            
            Example e = new Example(input, output);
            
            testSet.add(e);
        } 
        test.close();
        
        if(featerList != null) {    // extract only the selected features
            testSet = Example.updateExampleInput(featerList, testSet);
        }
        
        double testAccuracy = ann.predictionAccuracy(testSet);
        System.out.printf("Test Acc = %.3f\n", testAccuracy);
    }
    
}
