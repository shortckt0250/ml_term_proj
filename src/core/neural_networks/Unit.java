package core.neural_networks;

/**
 * Represents a general unit in a artificial neural network. 
 * @author meha
 *
 */
public interface Unit {
    public double computeOutput(double[] input) throws InputAndWeightLengthMismatchExeption;
    public double getWeightAt(int index);
    public void setWeightAt(int index, double value);
    public int sizeOfWeightArray();
}
