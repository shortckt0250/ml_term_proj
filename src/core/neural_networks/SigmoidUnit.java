package core.neural_networks;

import java.util.Arrays;

public class SigmoidUnit implements Unit {
    Double output;
    double[] weights;
    
    public SigmoidUnit(double[] weights) {
        this.weights = weights;
    }
    
    
    @Override
    public double computeOutput(double[] input) throws InputAndWeightLengthMismatchExeption {
        if(input.length+1 != weights.length) {
            throw new InputAndWeightLengthMismatchExeption();
        }
        
        // summation
        double net = 0.0;
        for(int i = 0; i < weights.length; i++) {
            if(i == 0) {
                net += weights[i] * 1;
            } else {
                net += weights[i] * input[i-1];
            }
        }
        
        // sigmoid
        output = 1 / (1 + Math.pow(Math.E, -1 * net));
        
        return output;
    }


    @Override
    public double getWeightAt(int index) {
        return weights[index];
    }


    @Override
    public void setWeightAt(int index, double value) {
        weights[index] = value;
    }


    @Override
    public int sizeOfWeightArray() {
        return weights.length;
    }


    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        boolean flag = true;
        for(double w : weights) {
            if(flag) {
                sb.append(String.format("%.3f", w));
                flag = false;
            } else {
                sb.append(String.format(",%.3f", w));
            }
        }
        return "SigmoidUnit [weights=" + sb.toString() + "]";
    }
    
    
    
}
