package core.neural_networks;

import java.util.List;

public class DiscreteInputOutput {
    /**
     * We represent the HIGH and LOW outputs as 0.9 and 0.1 instead of 1 and 0, because Sigmoid
     * units cannot produce these output values given finite weight. This intern forces 
     * the weights to grow without bound
     */
    public static double HIGH = 0.9;
    public static double LOW = 0.1;
    
    public static double[] conver1ToN(List<String> possibleValues, String value) {
        double[] discrete = new double[possibleValues.size()];
        int index = possibleValues.indexOf(value);
        for(int i = 0; i < discrete.length; i++) {
            discrete[i] = i == index ? HIGH : LOW;
        }
        return discrete;
    }
    
    public static String converBackNTo1(List<String> possibleValues, double[] ouput) {
        int maxI = 0;
        for(int i = 0; i < ouput.length; i++) {
            if(ouput[i] > ouput[maxI]) 
                maxI = i;
        }
        
        return possibleValues.get(maxI);
    }
}
