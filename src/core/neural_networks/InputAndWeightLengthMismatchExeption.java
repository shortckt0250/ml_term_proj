package core.neural_networks;

public class InputAndWeightLengthMismatchExeption extends Exception {
    /**
     * Generated serial ID 
     */
    private static final long serialVersionUID = -4368107815914832355L;
    
    public InputAndWeightLengthMismatchExeption() { super(); }
    public InputAndWeightLengthMismatchExeption(String message) { super(message); }
    public InputAndWeightLengthMismatchExeption(String message, Throwable cause) { super(message, cause); }
    public InputAndWeightLengthMismatchExeption(Throwable cause) { super(cause); }
   
}
