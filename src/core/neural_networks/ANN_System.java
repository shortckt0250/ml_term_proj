package core.neural_networks;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

/**
 * Implementation of an artificial neural network with a layered architecture.
 * @author meha
 *
 */
public class ANN_System {
    public static final String STOP_CRITERIA = "stopCriteria";
    public static final String MOMENTUM = "momentum";
    public static final String LEARNING_RATE = "learningRate";
    public static final String HIDDEN = "hidden";
    /**
     * WEIGHT attributes in the GA hypothesis should be added last. This is because the number of WEIGHTs change with the value of #HIDDEN Units
     */
    public static final String WEIGHT = "weight";
    public static final String FEATURE_SELECTION = "feature";
    
    public enum OverfittingCorrection {
        NoCorrection, CrossValidation, K_FoldCrossValidation, WDecay
    }
    
    static final Random rng = new Random(675432);//Long.getLong("seed", System.currentTimeMillis())); 
    public static class Layer {
        Unit[] units;
        
        public Layer(int numOfUnitsInLayer) {
            units =  new Unit[numOfUnitsInLayer];
        }

        @Override
        public String toString() {
            return "Layer [" + Arrays.toString(units) + "]";
        }

    }
    
    /**
     * Represents the hidden and output layer in the network. DOES NOT INCLUDE INPUT LAYER.
     */
    Layer[] layers; 
    
    double learningRate;
    double momentum;
    int stopCriteria;
    int k_fold_num = 5;
    int[] numUnitsInLayers;
    List<String> targetValuesList;
    private boolean specificWeightsSpecified;
    private double[][] givenWeights;
    
    
    /**
     * Constructor 
     * @param numUnitsInLayers the length of numUnitsInLayers specifies the number of layers in the ANN. 
     * Where numUnitsInLayers[i] tell the number of units at layer i. Here Layer 0 is the input layer and layer 
     * (numUnitsInLayers.length-1) is the output. 
     */
    public ANN_System(int[] numUnitsInLayers, double learningRate, double momentum, int stopCriteria) {
        this.learningRate = learningRate;
        this.momentum = momentum;
        this.stopCriteria = stopCriteria;
        this.numUnitsInLayers = numUnitsInLayers;
        resetWeights();
    }

    /**
     * 
     * @param numUnitsInLayers
     * @param learningRate
     * @param momentum
     * @param stopCriteria
     * @param targetValuesList  - if the targetValue is discreet then the possible values need 
     * to be passed when using crossValidation.
     */
    public ANN_System(int[] numUnitsInLayers, double learningRate, double momentum, int stopCriteria, 
            List<String> targetValuesList) {
        this(numUnitsInLayers, learningRate, momentum, stopCriteria);
        this.targetValuesList = targetValuesList;
    }
    
    private void resetWeights() {
        if(specificWeightsSpecified) {
            this.setWeights(this.givenWeights);
        } else {
            this.layers = newWeights();
        }
    }
    
    private Layer[] newWeights() {
        Layer[] newLayers = new Layer[numUnitsInLayers.length-1];  // -1 because the input layer is not represented with units. it is implicit. 
        
        // initialise the weights of each unit
        for(int i = 0; i < newLayers.length; i++) {          
            newLayers[i] = new Layer(numUnitsInLayers[i+1]);
            for(int j = 0; j < numUnitsInLayers[i+1]; j++) {
                double[] weights = new double[numUnitsInLayers[i] + 1]; // #weights is the number of units in the previous layer plus 1(cuz of w0). 
                for(int k = 0; k < weights.length; k++) {
                    weights[k] = ((rng.nextDouble() / 10) * 2) - 0.1;
                }
                newLayers[i].units[j] = new SigmoidUnit(weights);
            }
        }
        
        return newLayers;
    }
    
    /**
     * Allows for the client to set the networks weight to specific values.  
     * @param weightsInput
     */
    public void setWeights(final double[][] weightsInput) {
        if(!specificWeightsSpecified) {
            specificWeightsSpecified = true;
            givenWeights = weightsInput;
        }
        
        Layer[] newLayers = new Layer[numUnitsInLayers.length-1];  // -1 because the input layer is not represented with units. it is implicit. 
        
        // initialise the weights of each unit
        for(int i = 0; i < newLayers.length; i++) {   //per layer       
            newLayers[i] = new Layer(numUnitsInLayers[i+1]);
            for(int j = 0; j < numUnitsInLayers[i+1]; j++) {    // per unit
                int numUnitsInPrevLayer = numUnitsInLayers[i] + 1;
                double[] weights = new double[numUnitsInPrevLayer]; // #weights is the number of units in the previous layer plus 1(cuz of w0). 
                for(int k = 0; k < weights.length; k++) {
                    int translatedIndex = (j*numUnitsInPrevLayer)+k;
                    weights[k] = weightsInput[i][translatedIndex];
                }
                newLayers[i].units[j] = new SigmoidUnit(weights);
            }
        }
        
        this.layers = newLayers;
    }
    
    /**
     * Learn the weights of each unit in the network from these given examples.
     * @param trainingSet - the data set to train this ANN on.
     * @param validationProportion - if Cross validation is going to be used then this specifies the proportion of 
     * the validations size to the total data set. Other wise set to null.
     * @param overfittingCorrectionType - 
     * @throws InputAndWeightLengthMismatchExeption
     */
    public void learn(List<Example> trainingSet, Double validationProportion, 
                    OverfittingCorrection overfittingCorrectionType, Double decayWeightRate) throws InputAndWeightLengthMismatchExeption {
        if(overfittingCorrectionType == OverfittingCorrection.NoCorrection) {
            learnAux(trainingSet, null, null);
        } else if(overfittingCorrectionType == OverfittingCorrection.CrossValidation) {
            List<Example> tList = new ArrayList<Example>();
            List<Example> vList = new ArrayList<Example>();
            for(int j = 0; j < trainingSet.size(); j++) {
                if(j < (1.0-validationProportion) * trainingSet.size()) {
                    tList.add(trainingSet.get(j));
                } else {
                    vList.add(trainingSet.get(j));
                }
            } 
            
            int optimal = learnAux(tList, vList, null);
            System.out.println("Optimal Iteration Cross-Validation= " + optimal);
        } else if(overfittingCorrectionType == OverfittingCorrection.K_FoldCrossValidation) {
            int optimalIteration = 0;
            int validationSize = trainingSet.size() / k_fold_num;
            for(int i = 0; i < k_fold_num; i++) {
                List<Example> tList = new ArrayList<Example>();
                List<Example> vList = new ArrayList<Example>();
                for(int j = 0; j < trainingSet.size(); j++) {
                    if(j >= i*validationSize && j < (i+1)*validationSize) {
                        vList.add(trainingSet.get(j));
                    } else {
                        tList.add(trainingSet.get(j));
                    }
                }
                
                
                optimalIteration += learnAux(tList, vList, null);
                resetWeights();
            }
            
            optimalIteration = optimalIteration / k_fold_num;
            this.stopCriteria = optimalIteration;
            learnAux(trainingSet, null, null);
            System.out.println("Optimal Iteration K_Fold = " + optimalIteration);
        } else if(overfittingCorrectionType == OverfittingCorrection.WDecay) {
            learnAux(trainingSet, null, decayWeightRate);
        }
        
        return;
    }
    
    /**
     * Learn the weights of each unit in the network from these given examples.
     * @param examples -  list examples to learn from.
     * @throws InputAndWeightLengthMismatchExeption 
     */
    private int learnAux(List<Example> examples, List<Example> validation, Double decayWeightRate) throws InputAndWeightLengthMismatchExeption {
        //System.out.println("Learning...");
        
        int optimalIterations = stopCriteria;
        Layer[] optimalLayerWeights= null;
        Double optimalSquaredError = null;
        
        double[][][] prevWDiff = null;
        
        // until stop criteria
        for(int iter = 0; iter <= this.stopCriteria; iter++) {
            // for all examples
            for(Example ex : examples) {
                // PHASE 1: propagate forward
                List<double[]> outputs = forwardPropagate(ex);
                
                
                // PHASE 2: propagate backward
                int numOutputUnits = layers[layers.length- 1].units.length;
                double[] outputErrors = new double[numOutputUnits];
                
                // for each output unit calculate error term
                for(int k = 0; k < numOutputUnits; k++) {
                    double Ok = outputs.get(outputs.size()-1)[k];
                    outputErrors[k] = Ok * (1.0 - Ok) * (ex.getOutputAt(k) - Ok);
                }
                
                List<double[]> errors = new ArrayList<double[]>();
                errors.add(0, outputErrors);
                
                double[] mPlus1Erros = outputErrors;
                
                // for each hidden layer calculate error term
                for(int m = layers.length-2; m >= 0; m--) { // for each hidden layer m
                    int numberUnitsInLayer = layers[m].units.length;
                    double[] layerOutputErrors = new double[numberUnitsInLayer];
                    
                    for(int r = 0; r < numberUnitsInLayer; r++) {   // for each unit r in layer m
                        double Or = outputs.get(m)[r];//outputs.get(outputs.size()-1)[r];
                        double summationTerm = 0.0;
                        for(int s = 0; s < layers[m+1].units.length ; s++) {    // for each weight in unit r
                            //summationTerm += layers[m+1].units[m+1].getWeightAt(s) * mPlus1Erros[s];
                            summationTerm += layers[m+1].units[s].getWeightAt(r+1) * mPlus1Erros[s];
                        }
                        
                        layerOutputErrors[r] = Or * (1 - Or) *summationTerm;
                    }
                    
                    errors.add(0, layerOutputErrors);
                    mPlus1Erros = layerOutputErrors;
                }
                
                // update weights
                for(int l = 0; l < layers.length; l++) {    // for each layer in system.
                    for(int u = 0; u < layers[l].units.length; u++) {   // for each unit in layer l.
                        for(int w = 0; w < layers[l].units[u].sizeOfWeightArray(); w++) { // for each weight in unit u.
                            double wDiff;
                            if(l == 0) { // the first hidden layer takes Xji from the input it self 
                                if(w == 0) {
                                    wDiff = this.learningRate * errors.get(l)[u] * 1; // because x0 = 1
                                } else {
                                    wDiff = this.learningRate * errors.get(l)[u] * ex.getInputAt(w-1);
                                }
                            } else { // the input to layer l comes from the output of layer l-1
                                if(w == 0) {
                                    wDiff = this.learningRate * errors.get(l)[u] * 1;   // because x0 = 1
                                } else {
                                    wDiff = this.learningRate * errors.get(l)[u] * outputs.get(l-1)[w-1];
                                }
                            }
                            
                            
                            // add momentum
                            if(prevWDiff == null) {
                                prevWDiff = createWDiffContainer();
                            } else {
                                wDiff = wDiff + this.momentum * prevWDiff[l][u][w];
                                prevWDiff[l][u][w] = wDiff;     // Remember the current diff as previous from now on.
                            }
                            
                            // update the weight
                            layers[l].units[u].setWeightAt(w, layers[l].units[u].getWeightAt(w) + wDiff);
                        }
                    }
                }
                
                
            }
             // if cross validation or k-fold remember optimal weights
            if(validation != null) {
                if(optimalSquaredError == null) {
                    optimalSquaredError = squaredErrorOn(validation);
                    //optimalIterations = iter;
                    optimalLayerWeights = newWeights();
                    copyWeights(layers, optimalLayerWeights);
                } else {
                    double currentSquaredError = squaredErrorOn(validation);
                    if(currentSquaredError < optimalSquaredError) { // if current error is less than optimalAccuracy error so far then
                        optimalSquaredError = currentSquaredError;
                        optimalIterations = iter;
                        copyWeights(layers, optimalLayerWeights);
                    }
                }
                //if(iter % 1000 == 0)
                //    System.out.println(iter + "\t" + optimalIterations);
            }
            
            if(decayWeightRate != null) {
                for(int l = 0; l < layers.length; l++) {    // for each layer in system.
                    for(int u = 0; u < layers[l].units.length; u++) {   // for each unit in layer l.
                        for(int w = 0; w < layers[l].units[u].sizeOfWeightArray(); w++) { // for each weight in unit u.
                            // apply decay to each weight
                            layers[l].units[u].setWeightAt(w, layers[l].units[u].getWeightAt(w) * decayWeightRate);
                        }
                    }
                }
            }
        }
        //System.out.println("Done Learning!");
        
        // save optimal weight.
        if(validation != null && optimalSquaredError != null) {
            copyWeights(optimalLayerWeights, layers);
        }
        
        return optimalIterations;
    }

    private double[][][] createWDiffContainer() {
        double[][][] prevWDiff = new double[layers.length][][]; 
        
        for(int l = 0; l < layers.length; l++) {    // for each layer in system.
            prevWDiff[l] = new double[layers[l].units.length][];
            for(int u = 0; u < layers[l].units.length; u++) {   // for each unit in layer l.
                prevWDiff[l][u] = new double[layers[l].units[u].sizeOfWeightArray()];
                
            }
        }
        
        return prevWDiff;
    }

    /**
     * Propagates the given input through the ANN.
     * @param ex the example containing the inputs.
     * @return
     * @throws InputAndWeightLengthMismatchExeption 
     */
    private List<double[]> forwardPropagate(Example ex) throws InputAndWeightLengthMismatchExeption {
        List<double[]> outputs = new ArrayList<double[]>();
        
        
        double[] inputs = ex.getInput();
        for(int l = 0; l < layers.length; l++) {    //for each layer
            double[] layerOutput = new double[layers[l].units.length];
            for(int s = 0; s < layers[l].units.length; s++) { // for each unit
                layerOutput[s] = layers[l].units[s].computeOutput(inputs);
            }
            
            outputs.add(layerOutput);
            inputs = layerOutput;
        }
        
        return outputs;
    }

    /**
     * Returns a copy of the given Layer[]
     * @param layers2 the Layer[] to be copeid.
     * @return a copy of the given Layer[]
     */
    private Layer[] copyLayerWeight(Layer[] layers2) {
        Layer[] copy = new Layer[layers2.length];
        for(int i = 0; i < copy.length; i++) {          // hidden layers start at i = 1.
            copy[i] = new Layer(numUnitsInLayers[i+1]);
            for(int j = 0; j < numUnitsInLayers[i+1]; j++) {    // each unit
                double[] weights = new double[numUnitsInLayers[i] + 1]; // #weights is the number of units in the previous layer plus 1(cuz of w0). 
                for(int k = 0; k < weights.length; k++) {
                    weights[k] = layers2[i].units[j].getWeightAt(k);
                }
                copy[i].units[j] = new SigmoidUnit(weights);
            }
        }
        return null;
    }
    
    private void copyWeights(Layer[] src, Layer[] dest) {
        for(int l = 0; l < src.length; l++) {    // for each layer in system.
            for(int u = 0; u < src[l].units.length; u++) {   // for each unit in layer l.
                for(int w = 0; w < src[l].units[u].sizeOfWeightArray(); w++) { // for each weight in unit u.
                    // copy from src to dest
                    dest[l].units[u].setWeightAt(w, src[l].units[u].getWeightAt(w));
                }
            }
        }
    }

    /**
     * 
     * @param dataSet the data set to calculate the accuracy on.
     * @return accuracy of this ANN on the given dataSet.
     * @throws InputAndWeightLengthMismatchExeption
     */
    public double predictionAccuracy(List<Example> dataSet) throws InputAndWeightLengthMismatchExeption {
        int correct = 0;
        // predict on test set
        for(Example ex : dataSet) {
            double[] prediction = this.predict(ex);
            
            String predictedTarget = DiscreteInputOutput.converBackNTo1(targetValuesList, prediction);
            String actualTarget = DiscreteInputOutput.converBackNTo1(targetValuesList, ex.getOutput());
            
            if(predictedTarget.equals(actualTarget))
                correct ++;
        }
        
        double accuracy = ((double)correct / (double)dataSet.size());
        return accuracy;
    }   
    
    /**
     * 
     * @param dataSet the data set to calculate the squaredError on.
     * @return squaredErrorOn of this ANN on the given dataSet.
     * @throws InputAndWeightLengthMismatchExeption
     */
    private double squaredErrorOn(List<Example> dataSet) throws InputAndWeightLengthMismatchExeption {
        double E = 0.0;
        // predict on test set
        // And calculate the square error.
        for(Example ex : dataSet) {
            double[] prediction = this.predict(ex);
            
            for(int i = 0; i < prediction.length; i++) {
                E += Math.pow(ex.getOutputAt(i) - prediction[i], 2);
            }
        }
        
        E = 0.5 * E; 
        return E;
    }
    
    /**
     * Uses the learned ANN to predict the outcome of the given input.
     * @param ex an Example object containing the inputs.
     * @return  array holding the values of the output units of ANN.
     * @throws InputAndWeightLengthMismatchExeption is thrown in the case where the given 
     * input array length is different from the input length expected by ANN.
     */
    public double[] predict(Example ex) throws InputAndWeightLengthMismatchExeption {
        List<double[]> outputs = forwardPropagate(ex);
        
        return outputs.get(outputs.size() - 1);
    }
    
    /**
     * Uses the learned ANN to predict the outcome of the given input. 
     * But in this case it returns the output from all units in the ANN.
     * These outputs are organised in layers.
     * @param ex an Example object containing the inputs.
     * @return  List of arrays holding the output values of each unit(both hidden and output unit) in ANN.
     * @throws InputAndWeightLengthMismatchExeption is thrown in the case where the given 
     * input array length is different from the input length expected by ANN.
     */
    public List<double[]> predictWithHiddenUnits(Example ex) throws InputAndWeightLengthMismatchExeption {
        List<double[]> outputs = forwardPropagate(ex);
        
        return outputs;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "ANN_System [" + Arrays.toString(layers) + "]";
    }

}
