package core.neural_networks;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.HashMap;
import java.util.List;


public class Example {
    double[] input;
    double[] output;
    
    
    public Example(int inputLength, int outputLength) {
        super();
        this.input = new double[inputLength];
        this.output = new double[outputLength];
    }
    
    public Example(double[] input, double[] output) {
        super();
        this.input = input;
        this.output = output;
    }
    
    public void setInputAt(int index, double value) {
        input[index] = value;
    }
    
    public void setOutputAt(int index, double value) {
        output[index] = value;
    }
    
    public double getInputAt(int index) {
        return input[index];
    }
    
    /**
     * @return the input
     */
    public double[] getInput() {
        return input;
    }
    
    public double[] getOutput() {
        return output;
    }
    
    public double getOutputAt(int index) {
        return output[index];
    }

    
    public static HashMap<String, Double[]> oneToN_MapInput(List<String> possibleValues) {
        HashMap<String, Double[]> oneToN = new HashMap<String, Double[]>();
        
        return oneToN;
    }
    
    public static HashMap<String, Double[]> oneToN_MapOutput(List<String> possibleValues) {
        HashMap<String, Double[]> oneToN = new HashMap<String, Double[]>();
        
        return oneToN;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Example [input=" + Arrays.toString(input) + ",\n output="
                + Arrays.toString(output) + "]";
    }

    /**
     * Changes the provided Example list so that only Features with weight value featerList of 0.5 or more 
     * are included in input.
     * @param featerList
     * @param testSet
     * @return the new size of input
     */
    public static List<Example> updateExampleInput(List<Object> featerList,
            List<Example> examples) {
        
        BitSet selectedFeatures = new BitSet(featerList.size()); 
        for(int j =0 ;j < featerList.size(); j++) {
            double d = (Double) featerList.get(j);
            if(d >= 0.5) { // then select feature j
                selectedFeatures.set(j);
            }
        }
        
        List<Example> result = new ArrayList<Example>(examples.size());
        for(int i = 0; i < examples.size(); i++) {
            Example e = examples.get(i);
            double[] selectedInput = new double[selectedFeatures.cardinality()];
            int index = 0;
            for(int f = selectedFeatures.nextSetBit(0); f > 0; f = selectedFeatures.nextSetBit(f+1)) {
                selectedInput[index] = e.getInputAt(f);
            }
            //examples.set(i, new Example(selectedInput, e.getOutput()));
            result.add(new Example(selectedInput, e.getOutput()));
        }
        
        return result;
    }

}
