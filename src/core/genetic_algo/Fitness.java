package core.genetic_algo;

import java.util.List;

import core.neural_networks.Example;
import core.neural_networks.InputAndWeightLengthMismatchExeption;

public interface Fitness {
    double fitFunction(Hypothesis h, List<Example> training, List<Example> validation
            , int[] numUnitsInANNLayers, List<String> targetValuesList) throws InputAndWeightLengthMismatchExeption;
}
