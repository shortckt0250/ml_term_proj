package core.genetic_algo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;

import core.neural_networks.ANN_System;

public class Hypothesis implements Comparable<Hypothesis>{
    
    private double fitness;
    int numOfRules;
    int ruleLength;
    List<Rule> rules;

    public Hypothesis(int ruleLength, int numOfRules, List<Attribute> attribList) {
        rules = new ArrayList<Rule>();
        for(int i = 0; i < numOfRules; i++)
            rules.add(new Rule(ruleLength, attribList));
        
        this.ruleLength = ruleLength;
        this.numOfRules = numOfRules;
    }
    
    public Hypothesis(int ruleLength, ArrayList<Boolean> bitArray, List<Attribute> attribList) {
        this.ruleLength = ruleLength;
        this.numOfRules = bitArray.size() / ruleLength;
        
        rules = new ArrayList<Rule>();
        for(int i = 0; i < this.numOfRules; i++) {    // this.numOfRules-1 because i is used to calculate the start and start only need to go as far as i < this.numOfRules-1 
            int start = i*this.ruleLength;
            Rule r = new Rule(this.ruleLength, attribList);
            for(int j = start, k=0; j < start + this.ruleLength; j++, k++) {
                r.setBitAt(k, bitArray.get(j));
            }
            rules.add(r);
        }
    }

    /**
     * Generates a hypothesis with random bitString.
     * @param attribList 
     * @param size  - size of the hypothesis bitString. 
     */
    public static Hypothesis generateRandomHypothesis(int ruleLength, int numberOfRules, List<Attribute> attribList) {
        Hypothesis h = new Hypothesis(ruleLength, numberOfRules, attribList);
        h.initializeRandom(attribList);
        return h;
    }
    
    /**
     * @return the fitness
     */
    public double getFitness() {
        return fitness;
    }

    /**
     * @param fitness the fitness to set
     */
    public void setFitness(double fitness) {
        this.fitness = fitness;
    }
        
    /**
     * Initialises the hypothesis bitString with random rules. Only Valid rules are added.
     * @param attribList 
     * @param size  - size of the hypothesis bitString. 
     */
    private void initializeRandom(List<Attribute> attribList) {
        rules.clear();
        for(int i = 0; i < this.numOfRules; i++) {
            Rule r = Rule.generateRandomRule(this.ruleLength, attribList);
            // TODO an other option is to take an invalid rule and make it valid.
            while(!r.isValid()) {
                r.makeValid();// = Rule.generateRandomRule(this.ruleLength);
            }
            rules.add(r);
        }
    }

    /**
     * Predict the outcome of the given example based on the given example.
     * @param e - example to be predicted
     * @return prediction as a bitString.
     
    public boolean[] predict(Example e, boolean[] defaultPredic) {
        boolean[] predicted = defaultPredic;
        for(Rule r : rules) {
            boolean[] p = r.predict(e);
            if(p != null) {
                predicted = p;
                break;
            }
        }
        
        return predicted;
    }
    */
    
    
    /**
     * Checks if this hypothesis's rules are valid representation.
     * @return
     */
    public boolean isValid() {
        if(this.numOfRules == 0) 
            return false;
        for(Rule r : rules) {
            if(!r.isValid())
                return false;
        }
        
        return true;
    }

    /**
     * 
     * @return return the length of the bitString representation of this hypothesis.
     */
    public int getBitStringLength() {
        return this.numOfRules * this.ruleLength;
    }
    
    /**
     * @return the numOfRules
     */
    public int getNumOfRules() {
        return numOfRules;
    }

    /**
     * @param numOfRules the numOfRules to set
     */
    public void setNumOfRules(int numOfRules) {
        this.numOfRules = numOfRules;
    }

    /**
     * @return the ruleLength
     */
    public int getRuleLength() {
        return ruleLength;
    }

    /**
     * @param ruleLength the ruleLength to set
     */
    public void setRuleLength(int ruleLength) {
        this.ruleLength = ruleLength;
    }

    
    public boolean getBitAt(int index) {
        int rnum = index / this.ruleLength;
        int rindex = index % this.ruleLength;
        return rules.get(rnum).getBitAt(rindex);
    }
    
    public void setBitAt(int index, boolean value) {
        int rnum = index / this.ruleLength;
        int rindex = index % this.ruleLength;
        rules.get(rnum).setBitAt(rindex, value);
    }
    
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for(Rule r : this.rules) {
            sb.append(r.toString() + "\n");
        }
        return sb.toString();
    }
    
    /**
     * Sort the rules in this hypothesis based on their accuracy.
     * @param examples - data to calculate Accuracy on.
     
    public void sortRules(List<Example> examples) {
        // 
        for(Rule r : rules) {
            double correct = 0.0;
            double total = 0.0;
            for(Example e : examples) {
                boolean[] prediction = r.predict(e);
                if(prediction != null) {
                    total += 1;
                    if(Arrays.equals(e.getOutput(), prediction)) {
                        correct++;
                    }
                }
            }
            
            r.setAccuracy(correct / examples.size());
        }
        
        Collections.sort(this.rules);
    }
    */

    @Override
    public int compareTo(Hypothesis h) {
        int result = Double.compare(this.fitness, h.fitness);
        if(result == 0 ) {   // break tie by picking the hypothesis with smaller hidden number 
            Object ojThis = this.getValueOf(ANN_System.HIDDEN); 
            Object ojH = h.getValueOf(ANN_System.HIDDEN);
            if(ojThis != null && ojH != null) {
                result = -1 * Double.compare((Double)ojThis, (Double)ojH);  // the hypothesis with smaller num of hidden units is prefered
            }
        }
        
        return -1 * result; // becuase we want the hypothesis to be sorted in reverse order.
    }

    public Object getValueOf(String attribName) {
        Rule r = this.rules.get(0);
        
        return r.getValueOf(attribName);
    }

    public List<Object> getValueListOf(String attribName) {
        Rule r = this.rules.get(0);
        
        return r.getValueListOf(attribName);
    }
    
    /**
     * Extracts the ANN weights specified in genetic algorithm Hypothesis and returns then in the structure of the ANN. 
     * @param learnedHypothesis 
     * @param numUnitsInLayers - The ANN structure
     * @return
     */
    public double[][] extractWeights(int[] numUnitsInLayers) {
        // TODO Auto-generated method stub
        List<Object> list = this.getValueListOf(ANN_System.WEIGHT);
        
        double[][] w =  new double[numUnitsInLayers.length-1][];    //numUnitsInLayers.length-1 i/p layer does not have weights
        int index = 0;
        for(int i = 0; i < w.length; i++) {
            int layer = i+1;
            int numOfWeightsInLayer = (1 + numUnitsInLayers[layer-1]) * numUnitsInLayers[layer];
            w[i] = new double[numOfWeightsInLayer];
            for(int j = 0; j < w[i].length; j++) {
                w[i][j] = (Double) list.get(index);
                index++;
            }
        }
        
        return w;
    }
    
    /*
    public void pruneRules(List<Example> validationSet) {
        for(Rule r : this.rules) {
            r.pruneRule(validationSet);
        }
    }
    */
}
