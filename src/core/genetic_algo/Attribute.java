package core.genetic_algo;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;

/**
 * This can be a target attribute(output) or condition attribute (input)
 * @author meha
 *
 */
public class Attribute {
    public static Random rand = new Random(GeneticAlgorithm.SEED);//Long.getLong("seed", System.nanoTime()));
    /**
     * The following Four constants should be used for setting discreet type attributes allowed values. 
     * By default only single values are allowed.
     */
    public static final int ONLY_ONE_VALUE_ALLOWED = 0x00;  // 0001, 0010, 0100, 1000
    public static final int NO_VALUE_ALLOWED = 0x01;  // 0000   
    public static final int WILDCARD_VALUE_ALLOWED = 0x02;    // 1111
    public static final int MULTIPLE_VALUE_ALLOWED = 0x04;  //0011 or 0111, ....
    
    
    public enum AttributeType {
        target, condition
    }
    /**
     * 
     */
    String attribName;
    /**
     * The type of this attribute, Condition attribute or Target attribute.
     */
    AttributeType attrType;
    /**
     * The starting index value of this attribute in a given chromosome/hypothesis.
     */
    int startIndex;
    /**
     * the length of the bit representation of this attribute
     */
    int lengthOfValueBits;
    /**
     * True if this is a dicreetValue
     */
    boolean isDiscreet;
    /**
     * The valid Range if this is a continuous attribute
     */
    Range validRange;
    /**
     * this will be a flag with the NO_VALUE_ALLOWED, WILDCARD_VALUE_ALLOWED, MULTIPLE_VALUE_ALLOWED bits set or cleared.
     * By default only single values are allowed.
     */
    int discreetValidValues;
    /**
     * For discreet attributes this list will hold the Possible values of the attribute.
     * The ordering of these values is important, as it is used for translating from bitString to value and vice versa.
     */
    private List<String> valuesList;  
    
    private ValueConvertor convertor;
    
    
    /**
     * A constructor for discreet valued attributes.
     * @param valuesList - List of all the Possible values of the attribute.The ordering of these values is important, as it is used for translating from bitString to value and vice versa.
     * @param attrType - type of this attribute condition or target attribute
     * @param startIndex - the starting index of this attribute in the hypothesis bitString.
     */
    public Attribute(String attribName, List<String> valuesList, AttributeType attrType, 
            int startIndex, int discreetValidValues) {
        this.attribName = attribName;
        this.valuesList = valuesList;
        this.isDiscreet = true;
        this.lengthOfValueBits = valuesList.size();
        this.attrType = attrType;
        this.startIndex = startIndex;
        this.discreetValidValues = discreetValidValues;
        this.validRange = null;
    }
    
    /**
     * A constructor for continues valued attributes.
     * @param validRange - range of valid values.
     * @param attrType - type of this attribute condition or target attribute
     * @param startIndex - the starting index of this attribute in the hypothesis bitString.
     * @param convertor - A ContinuesValueConvertor object to be used to convert the values of this attribute to bitstring and vice versa.
     * @param lengthOfValueBits - the length of the bit representation of this attribute
     */
    public Attribute(String attribName, Range validRange, AttributeType attrType,
            int startIndex, ValueConvertor convertor, int lengthOfValueBits) {
        this.attribName = attribName;
        this.validRange = validRange;
        this.attrType = attrType;
        this.isDiscreet = false;
        this.startIndex = startIndex;
        this.convertor = convertor;
        this.validRange = validRange;
        this.lengthOfValueBits = lengthOfValueBits;
    }
    
    /**
     * @return the startIndex
     */
    public int getStartIndex() {
        return startIndex;
    }

    /**
     * @param startIndex the startIndex to set
     */
    public void setStartIndex(int startIndex) {
        this.startIndex = startIndex;
    }
    
    public void makeRuleAttributeValid(Rule rule) {
        
        if(!this.isDiscreet) {  // if continues
            //boolean[] tmpArr = new boolean[this.lengthOfValueBits];
            //for(int i = this.startIndex;  i < this.startIndex + tmpArr.length; i++) {
            //    tmpArr[i] = rule.getBitAt(i);
            //}
            
            // if value is out side valid range
            BitSet attrVal = rule.getBitString().get(this.getStartIndex(), this.startIndex + this.lengthOfValueBits);
            double val = ((ContiniousValue)this.convertor.toActualValue(attrVal, this.lengthOfValueBits)).getValue();
            if(!this.validRange.isInRange(new Double(val))) {
                // use modulus operation to get a valid value
                double min = (Double) this.validRange.getMinValue(); 
                double max = (Double) this.validRange.getMaxValue();
                
                if(val < min) {
                    if(min < 0.0) {
                        val = mod(val, min);
                    } else {
                        if(val < 0.0) { // if val is negative and min is not negative then simply rever sign on val
                            val *= -1;
                        } else {    // if val is not negative and min is not negative, then scale val by min
                            val += min;
                        }
                    }
                } else if(val > max) {
                    val = mod(val, max);
                } else {
                    // should never happen.
                }
                
                // convert the new value to bit[]  and set the rule
                boolean[] bitConv = this.convertor.toBitStringRepresentation(new ContiniousValue(val));
                for(int i = this.startIndex;  i < this.startIndex + this.lengthOfValueBits; i++) {   
                    rule.setBitAt(i, bitConv[i-this.startIndex]);
                }                
            }
        } else {    // if discrete
            int signStartIndex = this.startIndex + 0;
            int cardinality = 0;
            
            int lastSetBitIndex = 0;
            ArrayList<Integer> setIndices = new ArrayList<Integer>();
            for(int i = signStartIndex; i < signStartIndex + this.lengthOfValueBits; i++) {
                if(rule.getBitAt(i)) {
                    cardinality++;
                    lastSetBitIndex = i;
                    setIndices.add(i);
                }
            }
            
            if(cardinality == 0) {
                if((this.discreetValidValues & NO_VALUE_ALLOWED) == 0) { // if not allowed
                    int rIndex = rand.nextInt(this.lengthOfValueBits) + signStartIndex;
                    rule.setBitAt(rIndex, true);
                }
            } else if(cardinality > 1 && cardinality < this.lengthOfValueBits) {
                if((this.discreetValidValues & this.MULTIPLE_VALUE_ALLOWED) == 0) { // if not allowed
                    int rIndex = setIndices.get(rand.nextInt(setIndices.size())) ;
                    for(int i = signStartIndex; i < signStartIndex + this.lengthOfValueBits; i++) {
                        if(i != rIndex) {//lastSetBitIndex) {
                            rule.setBitAt(i, false);
                        }
                    }
                }
            } else if(cardinality == this.lengthOfValueBits) {
                if((this.discreetValidValues & this.WILDCARD_VALUE_ALLOWED) == 0) { // if not allowed
                    int rIndex = rand.nextInt(this.lengthOfValueBits) + signStartIndex;
                    for(int i = signStartIndex; i < signStartIndex + this.lengthOfValueBits; i++) {
                        if(i != rIndex) {
                            rule.setBitAt(i, false);
                        }
                    }
                }
            } else {
                // cardinality ==  1 and its valid. 
            }
        }
    }
    
    /**
     * Modulo operation for bot +ve and -ve values.
     * @param val
     * @param min
     * @return
     */
    private double mod(double val, double denom) {
        boolean isNegative = (val < 0.0);
        
        double absVal = Math.abs(val);
        double absDenom = Math.abs(denom);
        
        absVal = absVal%absDenom;
        
        if(isNegative)
            absVal *= -1;
        
        return absVal;
    }

    public boolean isValidValue(BitSet value) {
        // if continues attrib AND if range is defined  first convert to actual value using ContinuesValueConvertor convertor,
        if(!this.isDiscreet) {
            //boolean[] tmpArr = new boolean[lengthOfValueBits];
            //for(int i = 0;  i < tmpArr.length; i++) {
            //    tmpArr[i] = value.get(i);
            //}
            
            
            double val = ((ContiniousValue)this.convertor.toActualValue(value, this.lengthOfValueBits)).getValue();
            return this.validRange.isInRange(new Double(val));
        } else {
            // else if discrete attrib, then check as is but be mindful of discreetValidValues defined.
            int cardinality = value.cardinality();
            if(cardinality == 0 ) {
                if((this.discreetValidValues & this.NO_VALUE_ALLOWED) > 0)
                    return true;
                else
                    return false;
            } else if(cardinality == 1) {
                    
                if(value.nextSetBit(0) >=0 && value.nextSetBit(0) < this.valuesList.size())    // if it maps to an index in valuesList the okay
                    return true;
                else
                    return false;
            } else {
                for(int i = value.nextSetBit(0); i >= 0; i = value.nextSetBit(i+1)) {
                    if(i >=0 && i < this.valuesList.size())
                        continue;
                    else
                        return false;
                }
                
                if(cardinality == this.valuesList.size()) {
                    if((this.discreetValidValues & this.WILDCARD_VALUE_ALLOWED) > 0) {
                        return true;
                    } else {
                        return false;
                    }
                    
                } else {
                    if((this.discreetValidValues & this.MULTIPLE_VALUE_ALLOWED) > 0) {
                        return true;
                    } else {
                        return false;
                    }
                }
                
                
            }
        
        }
    }
    

    /**
     * @return the lengthOfValueBits
     */
    public int getLengthOfValueBits() {
        return lengthOfValueBits;
    }

    /**
     * @param lengthOfValueBits the lengthOfValueBits to set
     */
    public void setLengthOfValueBits(int lengthOfValueBits) {
        this.lengthOfValueBits = lengthOfValueBits;
    }
    
    /**
     * @return the attribName
     */
    public String getAttribName() {
        return attribName;
    }
    
    /**
     * @return the attrType
     */
    public AttributeType getAttrType() {
        return attrType;
    }

    /**
     * @return the converter
     */
    public ValueConvertor getConvertor() {
        return convertor;
    }

    /**
     * @param convertor the convertor to set
     */
    public void setConvertor(ValueConvertor convertor) {
        this.convertor = convertor;
    }
    
    public boolean isDiscreteAttribute() {
        return isDiscreet;
    }
    
    /**
     * 
     */
    public int indexOfValue(String value) {
        return this.valuesList.indexOf(value);
    }
    
    public String valueOfIndex(int index) {
        return this.valuesList.get(index);
    }

    public boolean isTargetAttribute() {
        return (this.attrType == AttributeType.target);
    }

    
}
