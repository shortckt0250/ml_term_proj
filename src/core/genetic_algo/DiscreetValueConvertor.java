package core.genetic_algo;

import java.util.BitSet;


public class DiscreetValueConvertor implements ValueConvertor<String> {

    Attribute attrib;
    
    public DiscreetValueConvertor(Attribute attrib) {
        this.attrib = attrib;
    }
    
    @Override
    public boolean[] toBitStringRepresentation(String value) {
        boolean[] bitstr = new boolean[attrib.getLengthOfValueBits()];
        
        int index = attrib.indexOfValue(value);
        
        bitstr[index] = true;
        
        return bitstr;
    }

    @Override
    public String toActualValue(BitSet bitString, int length) {
        StringBuilder sb = new StringBuilder();
        
        boolean first = true;
        for(int i = 0; i < length; i++) {
            if(bitString.get(i)) {
                sb.append(String.format("%s%s", attrib.valueOfIndex(i), 
                        ((!first) ? "&&" : "")));
                first = false;
            }
        }
        
        return sb.toString();
    }

}
