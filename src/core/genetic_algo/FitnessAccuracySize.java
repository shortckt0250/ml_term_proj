package core.genetic_algo;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import core.neural_networks.ANN_System;
import core.neural_networks.Example;
import core.neural_networks.InputAndWeightLengthMismatchExeption;

public class FitnessAccuracySize implements Fitness {
    
    @Override
    public double fitFunction(Hypothesis h, List<Example> training, List<Example> validation, 
            int[] numUnitsInANNLayers, List<String> targetValuesList) throws InputAndWeightLengthMismatchExeption {
        double fitness = 0.0;
        
        // create an ANN
        // Initialise parameters
        int[] numUnitsInLayers = numUnitsInANNLayers;
        double learningRate = 0.1;
        double momentum = 0.2;
        int stopCriteria = 10000;
        
        if(h.getValueOf("hidden") != null) {
            double d = (Double) h.getValueOf("hidden");
            numUnitsInLayers[1] = (int) d;
        }
        if(h.getValueOf("learningRate") != null) {
            learningRate = (Double) h.getValueOf("learningRate");
        }
        if(h.getValueOf("momentum") != null) {
            momentum = (Double) h.getValueOf("momentum");
        }
        if(h.getValueOf("stopCriteria") != null) {
            double d = (Double) h.getValueOf("stopCriteria");
            stopCriteria = (int) d;
        }
        List<Object> featerList = h.getValueListOf(ANN_System.FEATURE_SELECTION);
        if(featerList != null) {
            training = Example.updateExampleInput(featerList, training);
            validation = Example.updateExampleInput(featerList, validation);
            if(training.get(0) != null)
                numUnitsInLayers[0] = training.get(0).getInput().length; 
        }
        
        
        ANN_System ann = new ANN_System(numUnitsInLayers, learningRate, momentum, stopCriteria, targetValuesList);
        
        if(h.getValueOf(ANN_System.WEIGHT) != null) {
            double[][] weights = h.extractWeights(numUnitsInLayers);
            ann.setWeights(weights);
        }
        
        // learn on training set
        ann.learn(training, null, ANN_System.OverfittingCorrection.NoCorrection, null);
        
        // calculate fitness on valivation set
        
        
        fitness = Math.pow(ann.predictionAccuracy(validation), 1);// / h.getNumOfRules();
        //fitness = correct / (double)examples.size();
               
        //return ( fitness == 1.0 ? fitness-0.3 : fitness);
        return fitness;
    }

}
