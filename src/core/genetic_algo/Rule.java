package core.genetic_algo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.List;
import java.util.Random;

import core.genetic_algo.Attribute.AttributeType;

public class Rule implements Comparable<Rule>{
    public static Random rand = new Random(GeneticAlgorithm.SEED);//Long.getLong("seed", System.nanoTime()));
    private BitSet bitString;
    private int ruleLength;
    List<Attribute> attribList;
    /**
     * Rule accuracy is used when sorting the rules in a give hypothesis.
     */
    private double accuracy;

    public Rule(int ruleLength, List<Attribute> attribList) {
        super();
        this.ruleLength = ruleLength;
        this.bitString = new BitSet();
        this.attribList = attribList;
    }

    public static Rule generateRandomRule(int ruleLength, List<Attribute> attribList) {
        Rule r = new Rule(ruleLength, attribList);
        r.initializeRandom();
        return r;
    }
    
    public BitSet getBitString() {
        return bitString;
    }

    public void setBitString(BitSet bitString) {
        this.bitString = bitString;
    }
    
    public boolean getBitAt(int index) {
        return bitString.get(index);
    }
    
    public void setBitAt(int index, boolean value) {
        bitString.set(index, value);
    }
    
    public int getRuleLength() {
        return ruleLength;
    }

    public void setRuleLength(int ruleLength) {
        this.ruleLength = ruleLength;
    }

    public double getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(double accuracy) {
        this.accuracy = accuracy;
    }
    
    /**
     * Initialises the hypothesis bitString with random values.
     * @param size  - size of the hypothesis bitString. 
     */
    private void initializeRandom() {
        for(int i = 0; i < ruleLength; i++) {
            bitString.set(i, rand.nextBoolean());
        }
    }
    
    /**
     * Checks if this rule's bitString is a valid representation.
     * @return
     */
    public boolean isValid() {
        // first make sure the number weights is equal to the (# i/p w + #o/p w)
        
        for(Attribute a : attribList) { // loop until the start of the attribute is not outside the rule length
            // check validity of both input and target attribute
            BitSet value = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
            if(!a.isValidValue(value))
                return false;
        }
        
        return true;
    }
    
    /**
     * If this is an invalid rule correct all the mistakes and convert the this rule into a Valid one.
     * @param attribList - List of attributes.
     */
    public void makeValid() {
        for(Attribute a : attribList) {
            // check validity of both input and target attribute
            BitSet value = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
            while(!a.isValidValue(value)) {
                a.makeRuleAttributeValid(this);
                
                value = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
            }
                
        }
    }
    
    /**
     * Calculates the number of bits required to represent a single rule.
     * @param attribList - list of attributes 
     * @return the number of bits required to represent a single rule.
     */
    public static int calculateRuleLength(List<Attribute> attribList) {
        // calculate the length differently for continues and discrete values.
        int ruleLen = 0;
        for(Attribute a : attribList) {
            //if(a.isTargetAttribute()) 
            //    continue;
            
            if(a.isDiscreteAttribute()) {   // Discrete attributes
                ruleLen += a.getLengthOfValueBits();
            } else {    //Continuous attribute.
             // TODO implement calculateRuleLength for continues value.
                ruleLen += ((ContniousValueConvertor)a.getConvertor()).TOTAL_LEN;
            }
        }
        
        return ruleLen;
    }

    /**
     * If possible, predict the output for the given example case. 
     * @param e -  the example containing input values
     * @return NULL if rule does not apply to give example. Otherwise, return the predicted output as boolean[].
     
    public boolean[] predict(Example e) {
        // for each attribute
        for(Attribute a:attribList ) {
            if(a.isTargetAttribute() ) {
                continue;
            }
            // if discrete 
            if(a.isDiscreteAttribute()) {
                // get the value of this attribute from example 
                boolean[] in = e.getAttributeValue(a.getAttribName());
                // get the value in rule
                BitSet ruleAttr = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
                if(in == null) {    // the example does not contain value.
                    if(ruleAttr.cardinality() == 0) {
                        continue;
                    } else {
                        return null;
                    }
                }
                // and the two values
                    // if result is 0, return null
                    // else continue;
                boolean result = false;
                for(int i = 0; i < in.length; i++) {
                    result = result | (in[i] & this.bitString.get(i));
                }
                
                if(result) {
                    continue;
                } else {
                    if(ruleAttr.cardinality() == 0)
                        continue;
                    else
                        return null;
                }
            } else {
                // get the value of this attribute from example
                double exampleValue = (double) 
                        ((ContiniousValue)e.getAttributeActualValue(a.getAttribName())).getValueLow();
                
                // get the value in rule
                BitSet ruleAttr = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
                boolean[] ruleAttrBooleanArr = new boolean[a.getLengthOfValueBits()];
                for(int i=0; i < ruleAttrBooleanArr.length; i++) {
                    ruleAttrBooleanArr[i] = ruleAttr.get(i);
                }
                
                ContiniousValue ruleContVal = (ContiniousValue) a.getConvertor().toActualValue(ruleAttrBooleanArr);
                
                if(ruleContVal.getSign() == ContiniousValue.Sign.equals) {
                     double diff = Math.abs(exampleValue - ruleContVal.getValueLow());
                     // if difference is less than the available precision, then equality is assumed.
                     if(diff < Math.pow(10, -1 * ContniousValueConvertor.PRECISION)) {
                         continue;
                     } else {
                         return null;
                     }
                } else if(ruleContVal.getSign() == ContiniousValue.Sign.greater) {
                    if(exampleValue > ruleContVal.getValueLow()) 
                        continue;
                    else 
                        return null;
                } else if(ruleContVal.getSign() == ContiniousValue.Sign.less) {
                    if(exampleValue < ruleContVal.getValueHigh()) 
                        continue;
                    else 
                        return null;
                } else if(ruleContVal.getSign() == ContiniousValue.Sign.between) {
                    if(exampleValue > ruleContVal.getValueLow() && exampleValue < ruleContVal.getValueHigh()) 
                        continue;
                    else 
                        return null;
                } else if(ruleContVal.getSign() == ContiniousValue.Sign.dont_care) {
                    continue;
                } else {
                    throw new IllegalArgumentException("Sign is not correctly determined.");
                }
            }
        }    
            
        Attribute target = null;
        for(Attribute a:attribList ) {
            if(a.getAttrType() == Attribute.AttributeType.target)
                target = a;
        }
        
        BitSet bs = this.bitString.get(target.getStartIndex(), target.getStartIndex() + target.getLengthOfValueBits());
        
        boolean[] prediction = new boolean[target.getLengthOfValueBits()];
        for(int i = 0; i < prediction.length; i++) {
            prediction[i] = bs.get(i);
        }
        
        return prediction;
    }
    */
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("[");
        //sb.append(this.bitString.toString());
        boolean attributeOne = true;
        for(int j = 0; j < attribList.size(); j++) {
            Attribute a = attribList.get(j);
            
            if(a.isTargetAttribute()) {
                if(a.isDiscreteAttribute()) {
                    sb.append(" --> ");
                    
                    // convert the values to String
                    BitSet ruleAttr = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
                    BitSet ruleAttrOneBit = new BitSet();
                    boolean first  = true;
                    
                    for(int i = ruleAttr.nextSetBit(0); i > -1; i = ruleAttr.nextSetBit(i+1)) {
                        ruleAttrOneBit.set(i);
                        String val = (String)a.getConvertor().toActualValue(ruleAttrOneBit, a.getLengthOfValueBits());
                        ruleAttrOneBit.clear(i);
                        sb.append(String.format("%s%s", (first == true ? "" : " | "), val));
                        first = false;
                    }
                    
                    if(first) 
                        sb.append("NO_VALUE");
                } else {
                 // get the value in rule
                    BitSet ruleAttr = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
                    
                    ContiniousValue ruleContVal = (ContiniousValue) a.getConvertor().toActualValue(ruleAttr, a.getLengthOfValueBits());//ruleAttrBooleanArr);
                    
                    sb.append(String.format("%s = %.4f", a.getAttribName(), ruleContVal.getValue())); 
                    
                }
            } else {
                if(!attributeOne)
                    sb.append(" && ");
                attributeOne = false;
                
                if(a.isDiscreteAttribute()) {
                    
                    sb.append(a.getAttribName() + "=");
                    
                    // convert the values to String
                    BitSet ruleAttr = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
                    
                    BitSet ruleAttrOneBit = new BitSet();
                    boolean first  = true;
                    
                    for(int i = ruleAttr.nextSetBit(0); i > -1; i = ruleAttr.nextSetBit(i+1)) {
                        ruleAttrOneBit.set(i);
                        String val = (String)a.getConvertor().toActualValue(ruleAttrOneBit, a.getLengthOfValueBits());
                        ruleAttrOneBit.clear(i);
                        sb.append(String.format("%s%s", (first == true ? "" : " | "), val));
                        first = false;
                    }
                    
                    if(first) 
                        sb.append("NO_VALUE");
                } else {
                    // get the value in rule
                    BitSet ruleAttr = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
                    
                    ContiniousValue ruleContVal = (ContiniousValue) a.getConvertor().toActualValue(ruleAttr, a.getLengthOfValueBits());
                    
                    sb.append(String.format("%s = %.4f", a.getAttribName(), ruleContVal.getValue()));
                }
            }
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public int compareTo(Rule o) {
        double diff = this.accuracy - o.accuracy;
        if(diff > 0)
            return -1;
        else if(diff < 0)
            return 1;
        else
            return 0;
    }

    public static int indexOfAttributeInAttribList(String attribName, List<Attribute> list) {
        int i = 0;
        for(Attribute a : list) {
            if(a.getAttribName().equals(attribName)) {
                return i;
            }
            i++;
        }
        return -1;
    }

    private int indexOfAttribute(String attribName) {
        return indexOfAttributeInAttribList(attribName, this.attribList);
    }
    
    public Object getValueOf(String attribName) {
        // TODO Auto-generated method stub
        
        int indexAttr = indexOfAttribute(attribName);
        if( indexAttr >= 0) {
            Attribute a = this.attribList.get(indexAttr);
            int fromIndex = a.getStartIndex();
            int toIndex = a.getStartIndex() + a.getLengthOfValueBits();
            BitSet b = this.bitString.get(fromIndex, toIndex);
            Object obj = a.getConvertor().toActualValue(b, a.getLengthOfValueBits());
            if(obj.getClass().equals(ContiniousValue.class)) {
                return ((ContiniousValue)obj).getValue();
            } else {
                return obj;
            }
            
        }
        
        return null;
    }
    
    public List<Object> getValueListOf(String attribName) {
        int indexAttr = indexOfAttribute(attribName);
        if( indexAttr >= 0) {
            List<Object> valueList = new ArrayList<Object>();
            
            for(int i = indexAttr; i < this.attribList.size(); i++) {
                Attribute a = this.attribList.get(i);
                if(a.getAttribName().equals(attribName)) {
                    int fromIndex = a.getStartIndex();
                    int toIndex = a.getStartIndex() + a.getLengthOfValueBits();
                    BitSet b = this.bitString.get(fromIndex, toIndex);
                    Object obj = a.getConvertor().toActualValue(b, a.getLengthOfValueBits());
                    if(obj.getClass().equals(ContiniousValue.class)) {
                        valueList.add(((ContiniousValue)obj).getValue());
                    } else {
                        valueList.add(obj);
                    }
                }
            }
            
            return valueList;
        }
        return null;
    }
    
    /*
    public void pruneRule(List<Example> validationSet) {
        //for(Attribute a : this.attribList) {
        int j = 0;
        for(Attribute a = this.attribList.get(j++); j < this.attribList.size(); a = this.attribList.get(j++)) {
            if(a.isTargetAttribute())
                continue;
            
            BitSet unchnagedValue = this.bitString.get(a.getStartIndex(), a.getStartIndex() + a.getLengthOfValueBits());
            
            // before change
            double accuracyBefore = accuracyOnExamplesThatApplyToRule(validationSet);
            
            // generalize rule by making making attrinute as a wild card
            for(int i = a.getStartIndex(); i < a.getStartIndex() + a.getLengthOfValueBits(); i++) {
                this.bitString.set(i, true);
            }
            
            if(this.isValid()) {
                // calculate accuracy on examples that this rule applies to.
                double accuracyAfter = accuracyOnExamplesThatApplyToRule(validationSet);
                
                if(accuracyAfter < accuracyBefore) {
                    // undo changes
                    for(int i = a.getStartIndex(); i < a.getStartIndex() + a.getLengthOfValueBits(); i++) {
                        this.bitString.set(i, unchnagedValue.get(i - a.getStartIndex()));
                    }
                }
                
            } else {
                // undo changes
                for(int i = a.getStartIndex(); i < a.getStartIndex() + a.getLengthOfValueBits(); i++) {
                    this.bitString.set(i, unchnagedValue.get(i - a.getStartIndex()));
                }
            }
        }
    }

    
    private double accuracyOnExamplesThatApplyToRule(List<Example> validationSet) {
        double total = 0.0;
        double correct = 0.0;
        for(Example e : validationSet) {
            boolean[] prediction = this.predict(e);
            if(prediction == null)
                continue;
            
            if(Arrays.equals(prediction, e.getOutput())) {
                correct ++; 
            } 
            total++;
        }
        
        double a = (total != 0.0 ? correct / total : 0);
        return a;
    }
    */
}