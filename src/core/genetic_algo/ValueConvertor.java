package core.genetic_algo;

import java.util.BitSet;

public interface ValueConvertor<T> {
    public boolean[] toBitStringRepresentation(T value);
    public T toActualValue(BitSet bitString, int length);
}
