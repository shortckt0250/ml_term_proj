package core.genetic_algo;

public class Range<T> {
    T minValue;
    boolean minInclusive = true;
    T maxValue;
    boolean maxInclusive = true;
    
    public Range(T minValue, boolean minInclusive, T maxValue,
            boolean maxInclusive) {
        super();
        this.minValue = minValue;
        this.minInclusive = minInclusive;
        this.maxValue = maxValue;
        this.maxInclusive = maxInclusive;
    }

    /**
     * Checks if the given value is with in a range.
     * @param value - 
     * @return True if value is with in range. False otherwise.
     */
    public boolean isInRange(T value) {
        Comparable<? super T> v = (Comparable<? super T>) value;
        
        if(minInclusive ) {
            if(v.compareTo(minValue) < 0)
                return false;
        } else {
            if(v.compareTo(minValue) <= 0)
                return false;
        }
        
        if(maxInclusive ) {
            if(v.compareTo(maxValue) > 0)
                return false;
        } else {
            if(v.compareTo(maxValue) >= 0)
                return false;
        }
        
        return true;
    }
    
    /**
     * @return the minValue
     */
    public T getMinValue() {
        return minValue;
    }

    /**
     * @param minValue the minValue to set
     */
    public void setMinValue(T minValue) {
        this.minValue = minValue;
    }

    /**
     * @return the maxValue
     */
    public T getMaxValue() {
        return maxValue;
    }

    /**
     * @param maxValue the maxValue to set
     */
    public void setMaxValue(T maxValue) {
        this.maxValue = maxValue;
    }
}
