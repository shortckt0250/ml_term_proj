package core.genetic_algo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.BitSet;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import core.neural_networks.Example;
import core.neural_networks.InputAndWeightLengthMismatchExeption;

public class GeneticAlgorithm {
    public static final long SEED = (long) (0.97359122951703 * Math.pow(10, 14));//825002;//(long) (Math.random() *1000000);//Long.getLong("seed", System.nanoTime());//
    
    public static Random rand = new Random(SEED);//Long.getLong("seed", System.nanoTime()));
    
    public enum SelectionStrategy {
        Fitness_Proportion, Tornament, Rank
    }
    
    public enum CrossoverType {
        SinglePoint, TwoPoint, Uniform
    }
    
    public enum HypothesisLengthType {
        Fixed_Length, Variable_Length
    }
    
    /**
     * The fitness function used in ranking potential hypothesis.
     */
    private Fitness fitness;
    /**
     * Stop when fitness of 90% or more has been achieved on training data. The threshold specifying Stop criteria.
     */
    private Double fitnessThreshold = null;
    /**
     * The number of hypothesis to be included in the population.
     */
    private int p;
    /**
     * The fraction of population to be replaced by crossover at each step.
     */
    private double r;
    /**
     * The mutation rate.
     */
    private double m;
    /**
     * hypothesis type, variable or fixed number of rules.
     */
    //HypothesisLengthType hypoType;
    /**
     * If Variable_Length is selected then the initial number of rules each hypothesis. Otherwise the number of rules in each hypo.
     */
    private int initialNumOfRules;
    /**
     * The length of each rule in this hypothesis.
     */
    private int ruleLength;
    /**
     * Selection strategy to used when selecting (1-r)*p hypothesis from the old generation to be added to new generation.
     */
    private SelectionStrategy selectionType;
    /**
     * Crossover type
     */
    private CrossoverType crossoverType;
    /**
     * Stop learning after maxIter number of generations. Even if fitnessThreshold is not reached
     */
    int maxIter;
    /**
     * If the best hypothesis does not change for convergeAfterIter number of generations then stop learning.
     */
    Integer convergeAfterIter;
    
    private List<Attribute> attribList;
    
    boolean[] defaultPrediction = null;

    private boolean verbous = false;
    
    /**
     * The ANNlayers. only the input and out put layers are needed, the hidden later will be set by the GA itself
     */
    int[] numUnitsInLayers;
    
    public GeneticAlgorithm(Fitness fitness, Double fitnessThreshold, int p,
            double r, double m, int ruleLength, SelectionStrategy selectionType,
            CrossoverType crossoverType, List<Attribute> attribList, int initialNumOfRules, boolean verbous, 
            int maxIter, Integer convergeAfterIter, int[] numUnitsInLayers) {
        super();
        this.fitness = fitness;
        this.fitnessThreshold = fitnessThreshold;
        this.p = p;
        this.r = r;
        this.m = m;
        this.ruleLength = ruleLength;
        this.selectionType = selectionType;
        this.crossoverType = crossoverType;
        this.attribList = attribList;
        this.initialNumOfRules = initialNumOfRules;
        this.verbous = verbous;
        this.maxIter = maxIter;
        this.convergeAfterIter = convergeAfterIter;
        this.numUnitsInLayers = numUnitsInLayers;
        
        //GeneticAlgorithm.rand = new Random(GeneticAlgorithm.SEED);
        //Attribute.rand = new Random(GeneticAlgorithm.SEED);
        //Rule.rand = new Random(GeneticAlgorithm.SEED);
        
        if(verbous)
            System.out.println("SEED = " + SEED);
    }

    public Hypothesis learn(List<Example> training, List<Example> validation, List<String> targetValuesList) throws InputAndWeightLengthMismatchExeption {
        // defaultPrediction = Example.mostCommonOutput(examples);

        // Initialise population hypothesisLength
        List<Hypothesis> population = new ArrayList<Hypothesis>();
        initPopulation(population);
        
        // Evaluate fitness for each h in population
        Hypothesis maxFitnessH = evaluatePopulationFitness(population, training, validation, targetValuesList);
        Hypothesis prevMacFitnessH = maxFitnessH;
        
        // loop
        int count = 0;
        int sameHCount = 0;
        while(count++ < this.maxIter) {
            // create ne gen
            List<Hypothesis> newGeneration = new ArrayList<Hypothesis>();
            
            // select
            select(newGeneration, population, maxFitnessH);
            
            // crossover
            crossover(newGeneration, population);
            
            // mutate
            mutate(newGeneration);
            
            // update
            population = newGeneration;
            
            Hypothesis tmpH = evaluatePopulationFitness(population, training, validation, targetValuesList);
            
            if(tmpH.getFitness() >= maxFitnessH.getFitness())
                maxFitnessH = tmpH;
            
            if(maxFitnessH == prevMacFitnessH) {    // compare referance
                sameHCount++;
            } else {
                prevMacFitnessH = maxFitnessH;
                sameHCount = 0;
            }
            
            //if fitnessThreshold is defined the stop on achieving that fitness value
            if(this.fitnessThreshold != null && this.fitnessThreshold <= maxFitnessH.getFitness()) {
                if(verbous)
                    System.out.println("Stopped on achieving fitnessThreshold @ gen " + count);
                break;
            }
            
            if(this.convergeAfterIter != null && sameHCount == this.convergeAfterIter) {
                if(verbous)
                    System.out.println("Stopped on achieving Convergence criteria @ gen " + count);
                break;
            } 
            
            if(verbous) {
                
                System.out.printf("Max Fitness = %.4f \t H = %s\tGeneration %d\n", 
                        maxFitnessH.getFitness(), maxFitnessH.toString(), count);
            }
        }
        
        // return fittest hypothesis in the population
        Hypothesis best = population.get(0);
        best = maxFitnessH;
        /*for(Hypothesis h : population) {
            if(maxFitness == h.getFitness()) {
                best = h;
                break;
            }
        }*/
        
        // sort the rules in the best hypothesis
        // best.sortRules(examples);
        
        return best;
    }

    /**
     * Select (1-r)*p members from population according to the chosen type of selection strategy.
     * @param newGeneration - the new generation
     * @param population - the old population
     * @param maxFitnesIndex 
     */
    private void select(List<Hypothesis> newGeneration, List<Hypothesis> population, Hypothesis maxFitnesIndex) {
        if(selectionType == SelectionStrategy.Fitness_Proportion) {
            selectFitnessProportional(newGeneration, population, maxFitnesIndex);
        } else if(selectionType == SelectionStrategy.Tornament) {
            selectTornament(newGeneration, population, maxFitnesIndex);
        } else if(selectionType == SelectionStrategy.Rank) {
            selectRank(newGeneration, population, maxFitnesIndex);
        }
    }

    /**
     * Probabilistically select (1-r)*p members from population according to the formula 9.1 in the book and add them to the newGeneration.
     * @param newGeneration - the new generation
     * @param population - the old population
     * @param maxFitnesIndex 
     */
    private void selectFitnessProportional(List<Hypothesis> newGeneration, List<Hypothesis> population, Hypothesis maxFitnesH) {
        int selectionSize = (int) (population.size() * (1.0 - r));
        double summation = 0.0;
        for(Hypothesis h : population) {
            summation += h.getFitness();
        }
        /**
         * To avoid selection the same hypothesis more than once.
         */
        BitSet alreadySelected = new BitSet();
        // Always include the fittest hypothesis in the next generation.
        newGeneration.add(maxFitnesH);
        
        while(newGeneration.size() < selectionSize) {
            double random = rand.nextDouble();
            
            double total = 0.0;
            for(int j = 0; j < population.size() ; j++) {
                if(random > total && random < total + population.get(j).getFitness()/summation) {
                    if(!alreadySelected.get(j) && population.get(j) != maxFitnesH) {
                        newGeneration.add(population.get(j));
                        alreadySelected.set(j);
                    }
                    break;
                }
                
                total += (population.get(j).getFitness()/summation);
            }
        }
    }
    
    private void OLDselectFitnessProportional(List<Hypothesis> newGeneration, List<Hypothesis> population) {
        int selectionSize = (int) (population.size() * (1.0 - r));
        double summation = 0.0;
        for(Hypothesis h : population) {
            summation += h.getFitness();
        }
        /**
         * To avoid selection the same hypothesis more than once.
         */
        BitSet alreadySelected = new BitSet();
        while(newGeneration.size() < selectionSize) {
            for(int j = 0; j < population.size() && newGeneration.size() < selectionSize; j++) {
                if(!alreadySelected.get(j)) {
                    double selectionProb = population.get(j).getFitness() / summation;
                    if(rand.nextDouble() < selectionProb) {
                        newGeneration.add(population.get(j));
                        alreadySelected.set(j);
                    }
                }
            }
        }
    }

    private void selectTornament(List<Hypothesis> newGeneration,
            List<Hypothesis> population, Hypothesis maxFitnesH) {
        int selectionSize = (int) (population.size() * (1.0 - r));
        double strongSelectionProb = 0.6;
        
        /**
         * To avoid selection the same hypothesis more than once.
         */
        BitSet alreadySelected = new BitSet();
        // Always include the fittest hypothesis in the next generation.
        newGeneration.add(maxFitnesH);
        
        while(newGeneration.size() < selectionSize) {
            // two hypothesis h1 and h2 are first chosen at random
            int index1 = rand.nextInt(population.size());
            while(alreadySelected.get(index1) || population.get(index1) == maxFitnesH) {
                index1 = rand.nextInt(population.size());
            }
            int index2 = rand.nextInt(population.size());
            while(alreadySelected.get(index2) || population.get(index2) == maxFitnesH) {
                index2 = rand.nextInt(population.size());
            }
            
            Hypothesis h1 = population.get(index1);
            Hypothesis h2 = population.get(index2);
            
            
            if(rand.nextDouble() < strongSelectionProb) {
                if(h1.getFitness() >= h2.getFitness()) {
                    newGeneration.add(h1);  // add the strong one
                    alreadySelected.set(index1);
                } else {
                    newGeneration.add(h2);  // add the strong one
                    alreadySelected.set(index2);
                }
            } else {
                if(h1.getFitness() >= h2.getFitness()) {
                    newGeneration.add(h2);  // add the weak one
                    alreadySelected.set(index2);
                } else {
                    newGeneration.add(h1);  // add the weak one
                    alreadySelected.set(index1);
                }
            }  
        }
    }
    
    private void selectRank(List<Hypothesis> newGeneration,
            List<Hypothesis> population, Hypothesis maxFitnesH) {
        int selectionSize = (int) (population.size() * (1.0 - r));
        // sort the population
        Collections.sort(population); // will be sorted in descending order because of the way comparator in hypothesis is implemented.
        
        /**
         * To avoid selection the same hypothesis more than once.
         */
        BitSet alreadySelected = new BitSet();
        // Always include the fittest hypothesis in the next generation.
        newGeneration.add(maxFitnesH);
        while(newGeneration.size() < selectionSize) {
            double random = rand.nextDouble();
            double total = 0.0;
            double common = 1.0 / (((double) population.size() * (population.size() + 1)) / 2.0); // =(n*n+1)/2
            for(int i=0; i < population.size(); i++) {
                if(random > total && random < total + (population.size() - i) * common) {
                    if(!alreadySelected.get(i) && population.get(i) != maxFitnesH) {
                        newGeneration.add(population.get(i));
                        alreadySelected.set(i, true);
                        break;
                    } else {
                        break; //try again
                    }
                }
                total += (population.size() - i) * common;
            }
        }
    }
    
    /**
     * Probabilistically select r*p/2 pairs from population, according to Pr(h) as given in eq 9.1 in the book. 
     * @param newGeneration
     * @param population
     */
    private void crossover(List<Hypothesis> newGeneration, List<Hypothesis> population) {
        double summation = 0.0;
        for(Hypothesis h : population) {
            summation += h.getFitness();
        }
        
        //for(int i = 0; i < this.r * p; i++) {
        while(newGeneration.size() < this.p) {
            // probabilistically select pair of hypothesis
            Hypothesis h1 = probabilisticChoseHypothesis(population, summation);
            Hypothesis h2 = probabilisticChoseHypothesis(population, summation);
            
            if(this.crossoverType == CrossoverType.SinglePoint) {
                crossoverSinglePoint(h1, h2, newGeneration);
            } else if(this.crossoverType == CrossoverType.TwoPoint) {
                crossoverTwoPoint(h1, h2, newGeneration);
            } else if(this.crossoverType == CrossoverType.Uniform) {
                crossoverUniform(h1, h2, newGeneration);
            }
        }
        
        
    }    

    /**
     * Probabilistically chose a hypothesis from population list. 
     * The probability is as per equation 9.1 from the book.
     * @param population - the hypothesis population.
     * @param summation - the denominator term of equation 9.1 from the book.
     * @return  the chosen hypothesis.
     */
    private Hypothesis probabilisticChoseHypothesis(
            List<Hypothesis> population, double summation) {
        double rnd = rand.nextDouble(); 
        double total = 0.0;
        
        for(Hypothesis h : population) {
            if(total < rnd && rnd < total + h.getFitness() / summation) {
                //System.out.println(h + "\t" + rnd);
                return h;
            }
            total = total + h.getFitness() / summation;
        }
        
        return population.get(population.size() - 1);
    }

    
    /**
     * Perform a single point crossover on two given hypothesis and add the result to newGeneration.
     * @param h1 - hypothesis 1
     * @param h2 - hypothesis 2
     * @param newGeneration - the two new offsprings will be added to this list.
     */
    private void crossoverSinglePoint(Hypothesis h1, Hypothesis h2, List<Hypothesis> newGeneration) {   
        int trials = 0;
       // do {
            // pick the single crossover point
            int index = rand.nextInt(h1.getBitStringLength());//Math.min(h1.bitString.length, h2.bitString.length));
            
            // need to make sure the bitString lengths are correct 
            Hypothesis ofsp1 = new Hypothesis(h1.getRuleLength(), h1.getNumOfRules(), this.attribList);
            Hypothesis ofsp2 = new Hypothesis(h1.getRuleLength(), h1.getNumOfRules(), this.attribList);
            
            // do the cross over.
            for(int i = 0; i < h1.getBitStringLength(); i++) {
                if(i <= index) {
                    ofsp1.setBitAt(i, h1.getBitAt(i));
                    ofsp2.setBitAt(i, h2.getBitAt(i));
                } else {
                    ofsp1.setBitAt(i, h2.getBitAt(i));
                    ofsp2.setBitAt(i, h1.getBitAt(i));
                }
            }
            
            
            // after crossover make sure the offsprings are valid
            if(ofsp1.isValid() && newGeneration.size() < this.p) {
                newGeneration.add(ofsp1);
            }
            
            if(ofsp2.isValid() && newGeneration.size() < this.p) {
                newGeneration.add(ofsp2);
            }
            
            return;
       // } while(newGeneration.size() < this.p && trials++ < 5); // trials++ < 5 -> if after 5 cross over attempts we end up with invalid then try other parents
    }
    
    private void crossoverTwoPoint(Hypothesis h1, Hypothesis h2, List<Hypothesis> newGeneration) {
        // pick two random points r1 and r2 which will be the crossover point chosen for the first parent.
        int r1 = rand.nextInt(h1.getBitStringLength());
        int r2 = rand.nextInt(h1.getBitStringLength());
        while(r2 ==r1) {
            r2 = rand.nextInt(h1.getBitStringLength());
        }
        
        if(r2 < r1) {
            int tmp = r1;
            r1 = r2;
            r2 = tmp;
        } 
        
        // Determine d1 and d2 based on r1 and r2. 
        // This will be the distance from the rule boundary immediatly to r1 abd r2's left.
        int d1 = r1 % h1.getRuleLength();
        int d2 = r2 % h1.getRuleLength();
        
        // decide on the cross over points for the second parent by
        // generating a list of allowed cross over points and pick one at random 
        class Pair{ 
            public final int p1;
            public final int p2;
            public Pair(int p1, int p2) {
                super();
                this.p1 = p1;
                this.p2 = p2;
            }
        }
        ArrayList<Pair> pairList = new ArrayList<Pair>();
        for(int i = 0; i < h2.numOfRules; i++) {
            int point1 = i*h2.getRuleLength() + d1;
            for(int j = i; j < h2.numOfRules; j++) {
                int point2 = j*h2.getRuleLength() + d2;
                if(point2 <= point1) 
                    continue;
                pairList.add(new Pair(point1, point2));
            }
        }
        
        if(pairList.isEmpty()) {
            return;
        }
        
        int index = rand.nextInt(pairList.size());
        int r3 = pairList.get(index).p1;
        int r4 = pairList.get(index).p2;
        
        // perform cross over
        ArrayList<Boolean> bitstring1 = new ArrayList<Boolean>();
        boolean copyfrom1 = true;
        for(int i = 0; i < h1.getBitStringLength();) {
            if(i == r1 && copyfrom1) {
                i = r3;
                copyfrom1 = false;
            } 
            if(i == r4 && !copyfrom1) {
                i = r2;
                copyfrom1 = true;
            } 
            
            if(copyfrom1) {
                bitstring1.add(h1.getBitAt(i));
            } else {
                bitstring1.add(h2.getBitAt(i));
            }
            
            i++;
        }
        
        ArrayList<Boolean> bitstring2 = new ArrayList<Boolean>();
        boolean copyfrom2 = true;
        for(int i = 0; i < h2.getBitStringLength();) {
            if(i == r3 && copyfrom2) {
                i = r1;
                copyfrom2 = false;
            } 
            if(i == r2 && !copyfrom2) {
                i = r4;
                copyfrom2 = true;
            } 
            
            if(copyfrom2) {
                bitstring2.add(h2.getBitAt(i));
            } else {
                bitstring2.add(h1.getBitAt(i));
            }
            
            i++;
        }
        
        Hypothesis ofsp1 = new Hypothesis(h1.getRuleLength(), bitstring1, this.attribList);
        Hypothesis ofsp2 = new Hypothesis(h2.getRuleLength(), bitstring2, this.attribList);
        
        
     // after crossover make sure the offsprings are valid
        if(ofsp1.isValid() && newGeneration.size() < this.p) {
            newGeneration.add(ofsp1);
        }
        
        if(ofsp2.isValid() && newGeneration.size() < this.p) {
            newGeneration.add(ofsp2);
        }
        
        return;
    }
    
    private void crossoverUniform(Hypothesis h1, Hypothesis h2, List<Hypothesis> newGeneration) {
        // generate mask at random
        int randBytesSize = (h1.getBitStringLength() % 8 == 0 ? h1.getBitStringLength() % 8 : (h1.getBitStringLength() % 8) + 1);
        byte[] randBytes = new byte[randBytesSize];
        rand.nextBytes(randBytes);
        BitSet mask = BitSet.valueOf(randBytes);
        
        // need to make sure the bitString lengths are correct 
        Hypothesis ofsp1 = new Hypothesis(h1.getRuleLength(), h1.getNumOfRules(), this.attribList);
        Hypothesis ofsp2 = new Hypothesis(h1.getRuleLength(), h1.getNumOfRules(), this.attribList);
        
        // do the cross over.
        for(int i = 0; i < h1.getBitStringLength(); i++) {
            if(mask.get(i)) {
                ofsp1.setBitAt(i, h1.getBitAt(i));
                ofsp2.setBitAt(i, h2.getBitAt(i));
            } else {
                ofsp1.setBitAt(i, h2.getBitAt(i));
                ofsp2.setBitAt(i, h1.getBitAt(i));
            }
        }
        
        
        // after crossover make sure the offsprings are valid
        if(ofsp1.isValid() && newGeneration.size() < this.p) {
            newGeneration.add(ofsp1);
        }
        
        if(ofsp2.isValid() && newGeneration.size() < this.p) {
            newGeneration.add(ofsp2);
        }
        
        return;
    }
    
    /**
     * Choose m members of the new generation with uniform probability. 
     * For each chosen hypothesis invert one randomly selected bit.
     * @param newGeneration
     */
    private void mutate(List<Hypothesis> newGeneration) {
        for(Hypothesis h : newGeneration) {
            if(rand.nextDouble() < this.m) {
                int index = rand.nextInt(h.getBitStringLength());
                h.setBitAt(index, !h.getBitAt(index));
                
                int maxTry = 4;
                while(!h.isValid() && maxTry-- > 0) {
                    h.setBitAt(index, !h.getBitAt(index));
                    index = rand.nextInt(h.getBitStringLength());
                    h.setBitAt(index, !h.getBitAt(index));
                }
                
                if(!h.isValid())    // if h is still invalid then simply give up on mutation of this hypothesis.
                    h.setBitAt(index, !h.getBitAt(index));
            }
        }
    }

    /**
     * Evaluate the fitness of easelectionSizech hypothesis in the given population based on the given training examples.
     * @param population - the population of hypothesis.
     * @param examples - the training examples.
     * @return the fitest hypothesis in this population.
     * @throws InputAndWeightLengthMismatchExeption 
     */
    private Hypothesis evaluatePopulationFitness(List<Hypothesis> population,
        List<Example> training, List<Example> validation, List<String> targetValuesList) throws InputAndWeightLengthMismatchExeption {
        
        Hypothesis maxFitnessH = null;
        
        for(int i = 0; i < population.size(); i++) {
            Hypothesis h = population.get(i);
            double fitness = this.fitness.fitFunction(h, training, validation, numUnitsInLayers, targetValuesList);
            h.setFitness(fitness);
            if(this.verbous){
                System.out.printf("\tFitness = %.4f \t H = %s\n", fitness, h.toString());
            }
            
            if(maxFitnessH == null)
                maxFitnessH = h;
            //if(fitness > maxFitnessH.getFitness()) {
            if(maxFitnessH.compareTo(h) > 0) {
                maxFitnessH = h;
            }
        }
        
        return maxFitnessH;
    }

    /**
     * Generate p number of hypothesis at random.
     * @param population
     */
    private void initPopulation(List<Hypothesis> population) {
        population.clear();
        for(int i = 0; i < this.p; i++) {
            Hypothesis h;
            //if(this.hypoType == HypothesisLengthType.Fixed_Length)
                h = Hypothesis.generateRandomHypothesis(this.ruleLength, initialNumOfRules, this.attribList);
            //else 
            //    h = Hypothesis.generateRandomHypothesis(this.ruleLength, initialNumOfRules, this.attribList);
            population.add(h);
        }
    }
}
