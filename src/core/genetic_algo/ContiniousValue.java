package core.genetic_algo;

public class ContiniousValue {
    private double value;
    
    public ContiniousValue(double value) {
        
        this.value = value;
    }
    
    /**
     * @return the value
     */
    public double getValue() {
        return value;
    }

    /**
     * @param value the value to set
     */
    public void setValue(double valueLow) {
        this.value = value;
    }
    
}
