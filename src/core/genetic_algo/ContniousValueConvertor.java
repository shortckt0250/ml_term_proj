package core.genetic_algo;

import java.util.BitSet;

public class ContniousValueConvertor implements ValueConvertor<ContiniousValue> {
    /**
     * The number of decimal points that can be represented using this converter.
     */
    public double PRECISION = 4;
    /**
     * Size of the value representation in bits. Excluding the +/- and the comparison signs.
     */
    static int VALUE_SIZE = 16;
    
    /**
     * The total length of the bit representation of this continues value
     */
    public int TOTAL_LEN = VALUE_SIZE + 1; // 1 bit reserved to indicate +/-
    
    //static final int START_LOW = (ContiniousValue.NUM_SIGNS + 1 + VALUE_SIZE) - 1;
    
    public ContniousValueConvertor() {
        
    }
    
    /**
     * Constructor
     * @param precision - after decimal point to be considered.
     * @param valueBitsLen  - number of bits used to represent the a value. This is excluding the +/- bit.
     */
    public ContniousValueConvertor(double precision, int valueBitsLen) {
        this.PRECISION = precision;
        this.VALUE_SIZE = valueBitsLen;
        this.TOTAL_LEN = VALUE_SIZE + 1;
    }
    
    @Override
    public boolean[] toBitStringRepresentation(ContiniousValue value) {
        // identify the sign
        boolean isNegative = false;
        double dValue = value.getValue();
        if(dValue < 0.0) {
            isNegative = true;
            dValue = -1.0 * dValue;
        }
        
        dValue = dValue * Math.pow(10, PRECISION);
        long lv = (long) dValue;
        
        
        boolean[] binary = new boolean[TOTAL_LEN];
        
        // sign bit of the number
        if(isNegative) {
            binary[0] = true;
        }
        
        // i > 0 because the bit at index 0 is the +/- bit for the low value.
        for(int i = binary.length - 1; i > 0 && lv > 0; i--) { 
            long bit = lv & 1;
            if(bit == 1) 
                binary[i] = true;
                
            lv = lv >> 1;
        }
        
        return binary;
    }

    @Override
    public ContiniousValue toActualValue(BitSet bitString, int length) {
        if(length != TOTAL_LEN) 
            throw new IllegalArgumentException("bitString length not eaqual to TOTAL_LEN.");
        
        boolean isNegativeLow = bitString.get(0);
                        
        long lv = 0;
        for(int i = 0 + 1; i <= length-1; i++) {
            lv = lv << 1;
            if(bitString.get(i)) {
                lv = lv | 1;
            }
            
            
        }
        
        double dValue = (double)lv / Math.pow(10, PRECISION);
        if(isNegativeLow) {
            dValue = -1.0 * dValue;
        }
                
        return new ContiniousValue(dValue);
    }
    
    

}
